var mainApp = angular.module("mainApp", ['ui.router','ngResource','ngCookies','selectize','daterangepicker','ngFileUpload','datatables']);

//JS variable
var messages = {
  loginParamError:"All fields are required",

}


mainApp.config(['$stateProvider',function($stateProvider) {
  $stateProvider.state('login', {
    url: '/',
    templateUrl: 'app/template/login.html',
    controller: 'loginController'
  }).state('master_admin', {
    url:'/master_admin',
    templateUrl: 'app/template/ma_dashboard.html',
    controller: 'masterAdminController'
  }).state('administrator', {
    url:'/administrator',
    templateUrl: 'app/template/admin_dashboard.html',
    resolve:{
      company_status: function($http){
        return $http.get(url.getAllCompanyStatus)
        .then(function(response){
          console.log(response);
          return response.data;
        })
      }
    },
    controller: 'administratorController'
  }).state('sales_person', {
    url:'/sales_person',
    templateUrl: 'app/template/sp_dashboard.html',
    controller: 'salesPersonController'
  }).state('administrator_add', {
    url:'/administrator_add',
    templateUrl: 'app/template/admin-registration.html',
    controller: 'administratorAddController'
  }).state('site_add', {
    url:'/site_add',
    templateUrl: 'app/template/create-site.html',
    resolve:{
      sites: function($http){
        return $http.get(url.getsitelists)
        .then(function(response){
          return response.data.list;
        })
      }
    },
    controller: 'siteAddController'
  }).state('sales_person_add', {
    url:'/sales_person_add',
    templateUrl: 'app/template/salesperson-registration.html',
    controller: 'salespersonAddController'
  }).state('customer_add', {
    url:'/customer_add',
    templateUrl: 'app/template/customer-registration.html',
    controller: 'customerAddController'
  }).state('tax_add', {
    url:'/tax_add',
    resolve:{
      taxes: function($http){
        return $http.get(url.gettaxs)
        .then(function(response){
          return response.data.tax_list;
        })
      }
    },
    templateUrl: 'app/template/tax.html',
    controller: 'taxAddController'
  }).state('contact_person_add', {
    url:'/contact_person_add',
    resolve:{
      companies: function($http){
        return $http.get(url.getcustomers)
        .then(function(response){
          return response.data.list;
        })
      }
    },
    templateUrl: 'app/template/contact-person-registration.html',
    controller: 'cpAddController'
  }).state('order', {
    url:'/order',
     resolve: {            
      customers: function($http){
          return $http.get(url.getcustomers)
          .then(function(response){
            return response.data.list;
          })
          },
      salesPersons: function($http){
          return $http.get(url.getsalespersons)
          .then(function(response){
            return response.data.list;
          })
          },
      sites: function($http){
          return $http.get(url.getsitelists)
          .then(function(response){
            return response.data.list;
          })
          },
        },
    templateUrl: 'app/template/create-order.html',
    controller: 'orderController'
  }).state('po', {
    url:'/po',
     resolve: {            
      pending_po_order: function($http){
          return $http.get(url.getpendingpoorder)
          .then(function(response){
            return response.data;
          })
          },           
      customers: function($http){
          return $http.get(url.getcustomers)
          .then(function(response){
            return response.data.list;
          })
          },
        receivedPO: function($http){
          return $http.get(url.getAllReceivedPO).then(function(response){
            return response.data;
          })
        }      
        },
    templateUrl: 'app/template/create-po.html',
    controller: 'poController'
  })
}]);
