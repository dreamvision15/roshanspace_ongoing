//localhost/ipaddress
//var host = "http://roshanspace.reverziel.com/";
var host = "http://localhost/";
var dir_path = "roshanspace_ongoing/api/v1/";

var ip = {
	api: host+dir_path	
};

var url = {
	login:ip.api+"login",
	createadmin:ip.api+"create/administrator",
	getAllAdmin:ip.api+"admin_list",
	createsp:ip.api+"create/sales_person",
	createcompanies:ip.api+"companies/create",
	createcontactperson:ip.api+"contactpersons/create",
	createorder:ip.api+"orders/create",	
	createsite:ip.api+"sites/create",
	createpo:ip.api+"po/create",
	createtax:ip.api+"tax/create",
	updateadmin:ip.api+"update/administrator",
	updatesalesperson:ip.api+"update/salesperson",
	updatecompany:ip.api+"update/company",
	updatesite:ip.api+"sites/update",
	updatetax:ip.api+"tax/update",
	updateContactPerson: ip.api+"contactpersons/update",
	file_upload_url:ip+"roshanspace_ongoing/api/upload.php",
	poUpload: host+"roshanspace_ongoing/api/upload.php",	
	getcustomers:ip.api+"companies",
	getsalespersons:ip.api+"salespersons",
	getsitelists:ip.api+"sites",
	getpendingpoorder:ip.api+"orders/pendingpo",
	getCompanyPendingPO:ip.api+"orders/companypendingpo/",
	getAllOrders:ip.api+"orders",
	getAllCompanies:ip.api+"companies",
	getAllSites:ip.api+"sites",
	getAllcp:ip.api+"contactpersons/",
	getAllReceivedPO:ip.api+"po/received",
	getAllCompanyStatus:ip.api+"company/all/status",
	gettaxs:ip.api+"tax/view",

};