var status_val = [{status: 1, name:'Active'},{status: 0, name:'Inactive'}];

//values
mainApp.value('status_val', status_val);   


//Services
mainApp.service('service1', function($http, $rootScope, $cookieStore, $location, $state,$q) {
  this.showMessage = function(status, msg){
    $("#msg1").html(status);
    $("#msg2").html(msg);    
    $('#messages').modal('show');
    setTimeout(function() {$('#messages').modal('hide');}, 5000);
  };
  //data handler according to role
  this.fetchData = function(role){
    var dashboardData = {};
    var defer;
    defer = $q.defer();
    $q.all([
    $http({url: url.getAllCompanies,method: 'GET'}).then(function(response) {
      //console.log(response);
      if(response.data.status === 'success'){
        dashboardData.companyData = response.data.list;
      } else {
        dashboardData.companyData = new Array();
      }
    }),
    $http({url: url.getAllSites,method: 'GET'}).then(function(response) {
      if(response.data.status === 'success'){
        dashboardData.siteData = response.data.list;
      } else {
        dashboardData.siteData = new Array();
      }
    }),
    $http({url: url.getAllOrders,method: 'GET'}).then(function(response) {
      console.log(response);
      if(response.data){
        //dashboardData.ordersData = response.data.list;
        dashboardData.ordersData = response.data;
      } else {
        dashboardData.ordersData = new Array();
      }
    }),
    ]).then(function() {
      defer.resolve(dashboardData);
    });
    return defer.promise;
    //return dashboardData;
  }
  //login handler
  this.loginHandler = function(detail, type_of_login){
    $cookieStore.put('user_email',detail.email);
    $cookieStore.put('loggedIn', 'true');
    $cookieStore.put('userRole', type_of_login);
    $cookieStore.put('userDetail',detail);
    $rootScope.fullName = detail.first_name+" "+detail.last_name;
    $rootScope.userRole = type_of_login;
    $rootScope.loggedIn = true;
    $("#wrapper").removeClass("removeMenuPadding");
    switch(type_of_login){
      case "master_admin":
        $state.go('master_admin');
        break;
      case "administrator":
        $state.go('administrator');
        break;
      case "sales_person":
        $state.go('sales_person');
        break;        
      default:
        alert("default redirect");  
      }
  };

//this will check user session is it expired or not
  this.checkSession = function(){
    if($cookieStore.get('loggedIn')){
    var detail = $cookieStore.get('userDetail');
    $rootScope.id = detail.id;
    $rootScope.fullName = detail.first_name+" "+detail.last_name;
    $rootScope.email = detail.email;
    $rootScope.userRole = $cookieStore.get('userRole');
    $rootScope.loggedIn = true;
    }else{
      //this.showMessage("Timeout", "Your Session is expired please Login Again.")
      $state.go('login');
    }
    
  };

  //logout handler
  this.logoutHandler = function(){
    $("#loading").show();
    $rootScope.email = "";
    $rootScope.userRole = "";
    $rootScope.loggedIn = false;
    //clear all cookies storage
    $cookieStore.remove('loggedIn');
    $cookieStore.remove('userDetail');
    $cookieStore.remove('userRole');
    $("#loading").hide();
    //$location.path("/");
    $state.go('login');
  };

  this.redirectHandler = function(){
    if($cookieStore.get('loggedIn')){
      switch($cookieStore.get('userRole')){
        case "master_admin":
        $state.go('master_admin');
        break;
      case "administrator":
        $state.go('administrator');
        break;
      case "sales_person":
        $state.go('sales_person');
        break;        
      default:
        alert("default redirect"); 
        }      
      }
    }
});

//loginController
mainApp.controller('loginController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state) {
$scope.viewReady = false;
service1.redirectHandler();  
//remove menu padding on login page
$("#wrapper").addClass("removeMenuPadding");
$("#loading").hide();
$scope.viewReady = true
  $scope.loginMe = function(){
    $("#loading").show();
    var email_id = $("#email").val(); 
    var password = $("#pass").val(); 
    var type_of_login = $('input[name=loginas]:checked', '#login_form').val();

    /*scope.$watch('name', function(newValue, oldValue) {
    scope.counter = scope.counter + 1;
    });*/
    if(email_id === "" || password === "" || type_of_login === ""){
      service1.showMessage("error", messages.loginParamError);
      $("#loading").hide();
      return;      
    }else{
      var parameter = "type_of_login="+type_of_login+"&email_id="+email_id+"&password="+password;
      $http({
          url: url.login,
          method: 'POST',
          data: parameter,
          headers: {
              "Content-Type": "application/x-www-form-urlencoded"
          }

      }).success(function(data){
          //console.log(data);
          $("#loading").hide();
          var status = data.status;
          if(status === "success"){//login successful
            service1.showMessage(status, data.message);            
            service1.loginHandler(data.detail, type_of_login);
          }else{
            $("#loading").hide();
            service1.showMessage(status, data.message);
          }
      }).error(function(error){
        $("#loading").hide();
          service1.showMessage("error", "Fail to connect.");
      });            
    }
  };


});


mainApp.controller('navController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state) {
  service1.checkSession();

//logout clicked
  $scope.logout  = function(){
    var r = confirm(" you really want to logout? ");
      if (r === true) {
        service1.logoutHandler();
      }
  };
  //Menu Handler


});



mainApp.controller('masterAdminController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state) {
  service1.checkSession();
  $scope.message = "Master Admin Dashboard";
  var allrequiredData = service1.fetchData();
  allrequiredData.then(function(allData){
    $scope.companyData = allData.companyData;
    $scope.ordersData = allData.ordersData;
    $scope.siteData = allData.siteData;
  });
});

mainApp.controller('administratorController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state, company_status, $resource) {
  service1.checkSession();
  $scope.message = "Administrator Dashboard";
  var allrequiredData = service1.fetchData();
  allrequiredData.then(function(allData){
    $scope.ordersData = allData.ordersData;
    $scope.siteData = allData.siteData;
  });
  var vm = this;
  //company data with status
  $resource(url.getAllCompanyStatus).query().$promise.then(function(company) {
      $scope.companyData = company;
  });

});

mainApp.controller('salesPersonController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state) {
  service1.checkSession();
  $scope.message = "Sales Person Dashboard";
  var allrequiredData = service1.fetchData();
  allrequiredData.then(function(allData){
    $scope.companyData = allData.companyData;
    $scope.ordersData = allData.ordersData;
    $scope.siteData = allData.siteData;
  });
});

//filter for Status Text
mainApp
.filter('statusText', [
    '$filter', function() {
        return function(input) {
            if(input == "1"){
              return "Active";
            }else{
              return "Inactive";
            }
        };
    }
]);


/*ADD OPERATIUON START*/
mainApp.controller('administratorAddController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state, status_val) {
  service1.checkSession();
  $scope.form_header = "Administrator Registration";
  $scope.form_type = "administrator";


  $scope.changePassword = false;

  /*var allrequiredData = service1.fetchData();
  allrequiredData.then(function(allData){    
    $scope.ordersData = allData.ordersData;
    $scope.siteData = allData.siteData;
  });*/
  $scope.registerAdmin = function(){
    $("#loading").show();
    var parameter = "first_name="+$scope.firstname+"&last_name="+$scope.lastname+"&email_id="+$scope.email+"&password="+$scope.user_password+"&contact_number="+$scope.contact+"&address="+$scope.address+"&status="+$("#status").val();
    //console.log(parameter);
      $http({
          url: url.createadmin,
          method: 'POST',
          data: parameter,
          headers: {
              "Content-Type": "application/x-www-form-urlencoded"
          }

      }).success(function(data){
          //console.log(data);
          $("#loading").hide();
          var status = data.status;
          if(status === "success"){
            service1.showMessage(status, data.message);
            $state.reload();
          }else{
            service1.showMessage(status, data.message);
          }
      }).error(function(error){
        $("#loading").hide();
          service1.showMessage("error", "Fail to connect.");
      });      
  };


  $scope.editTabClick = function(){
    $("#loading").show();
    $scope.admin_list = "";
    $scope.status_val = status_val;
    console.log("edit admin call");
    $http({url: url.getAllAdmin,method: 'GET'}).then(function(response) {
      //console.log(response);
      if(response.data.status === 'success'){
        $scope.admin_list = response.data.list;
        $("#loading").hide();
      } else {
        $scope.admin_list = new Array();
        $("#loading").hide();
      }
    })
  };

  //edit existing tab clicked
  $scope.editExisting = function(admin){
    console.log(admin);
    $scope.change_password = false;
    $scope.password_btn_text = "Reset";
    $scope.edit_header = "Edit Administrator";    
    $scope.user_id = admin.id;
    $scope.edit_firstname = admin.first_name;
    $scope.edit_lastname = admin.last_name;
    $scope.edit_address = admin.address;
    $scope.edit_email = admin.email_id;
    $scope.edit_contact = admin.contact_number;
    $scope.edit_status = admin.status; 
    $('#editAdminModal').modal('show');
  };

  //change password button click on edit form
  $scope.passwordToggle = function(){
    $scope.change_password = !$scope.change_password;
    if($scope.change_password){
      $scope.password_btn_text = "Cancel";
    }else{
      $scope.password_btn_text = "Reset";
    }
    console.log($scope.change_password);
  };


  //edit form submit
  $scope.editUser = function(){
    $("#loading").show();
    var dataObj = {
        id: $scope.user_id,
        change_password: $scope.change_password,
        first_name: $scope.edit_firstname,
        last_name: $scope.edit_lastname,
        contact_number: $scope.edit_contact,
        address: $scope.edit_address,
        status: $scope.edit_status
      }
    if($scope.change_password){//coz of ng-if dom is not compiled thats why we use jquery type fetching value for password entry
        dataObj.new_password = $("#edit_user_password").val(); 
    }

    $http({
      method: 'POST',
      url: url.updateadmin,
      data: dataObj
    }).then(function successCallback(response) {      
       $('#editAdminModal').modal('hide');      
       $("#loading").hide();
       service1.showMessage(response.data.status, response.data.message);
       //this will refresh list of administrators with latest data
       $scope.editTabClick();
    }, function errorCallback(response) {
        console.log("ERROR");
        service1.showMessage(response.data.status, response.data.message);
        $("#loading").hide();
    });

  };//editUser function end


});

mainApp.controller('salespersonAddController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state) {
  service1.checkSession();
  $scope.form_header = "Sales Person Registration";
  $scope.form_type = "sales_person";
  $scope.sp_list = "";
  $scope.changePassword = false;

  var allrequiredData = service1.fetchData();
  allrequiredData.then(function(allData){
    $scope.companyData = allData.companyData;
    $scope.siteData = allData.siteData;
  });
  $scope.registerSalesPerson = function(){
    $("#loading").show();
    var parameter = "first_name="+$scope.firstname+"&last_name="+$scope.lastname+"&email_id="+$scope.email+"&password="+$scope.user_password+"&contact_number="+$scope.contact+"&address="+$scope.address+"&status="+$("#status").val();
    //console.log(parameter);
      $http({
          url: url.createsp,
          method: 'POST',
          data: parameter,
          headers: {
              "Content-Type": "application/x-www-form-urlencoded"
          }

      }).success(function(data){
          //console.log(data);
          $("#loading").hide();
          var status = data.status;
          if(status === "success"){
            service1.showMessage(status, data.message);
            $state.reload();
          }else{
            service1.showMessage(status, data.message);
          }
      }).error(function(error){
        $("#loading").hide();
          service1.showMessage("error", "Fail to connect.");
      });
    }//register sp fn end

    $scope.editTabClick = function(){
    $("#loading").show();
    $scope.sp_list = "";
    $scope.status_val = status_val;
    console.log("edit admin call");
    $http({url: url.getsalespersons,method: 'GET'}).then(function(response) {
      //console.log(response);
      if(response.data.status === 'success'){
        $scope.sp_list = response.data.list;
        $("#loading").hide();
      } else {
        $scope.sp_list = new Array();
        $("#loading").hide();
      }
    })
  };

  //edit existing tab clicked
  $scope.editExisting = function(sp){
    console.log(sp);
    $scope.change_password = false;
    $scope.password_btn_text = "Reset";
    $scope.edit_header = "Edit Sales Person";    
    $scope.user_id = sp.id;
    $scope.edit_firstname = sp.first_name;
    $scope.edit_lastname = sp.last_name;
    $scope.edit_address = sp.address;
    $scope.edit_email = sp.email_id;
    $scope.edit_contact = sp.contact_number;
    $scope.edit_status = sp.status; 
    $('#editSpModal').modal('show');
  };

  //change password button click on edit form
  $scope.passwordToggle = function(){
    $scope.change_password = !$scope.change_password;
    if($scope.change_password){
      $scope.password_btn_text = "Cancel";
    }else{
      $scope.password_btn_text = "Reset";
    }
    console.log($scope.change_password);
  };


  //edit form submit
  $scope.editUser = function(){
    $("#loading").show();
    var dataObj = {
        id: $scope.user_id,
        change_password: $scope.change_password,
        first_name: $scope.edit_firstname,
        last_name: $scope.edit_lastname,
        contact_number: $scope.edit_contact,
        address: $scope.edit_address,
        status: $scope.edit_status
      }
    if($scope.change_password){//coz of ng-if dom is not compiled thats why we use jquery type fetching value for password entry
        dataObj.new_password = $("#edit_user_password").val(); 
    }

    $http({
      method: 'POST',
      url: url.updatesalesperson,
      data: dataObj
    }).then(function successCallback(response) {      
       $('#editSpModal').modal('hide');      
       $("#loading").hide();
       service1.showMessage(response.data.status, response.data.message);
       //this will refresh list of administrators with latest data
       $scope.editTabClick();
    }, function errorCallback(response) {
        console.log("ERROR");
        service1.showMessage(response.data.status, response.data.message);
        $("#loading").hide();
    });

  };//editUser function end


});

mainApp.controller('customerAddController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state) {
  service1.checkSession();  

  /*var allrequiredData = service1.fetchData();
  allrequiredData.then(function(allData){
    $scope.companyData = allData.companyData;    
  });*/

  $scope.registerCustomer = function(){
    $("#loading").show();
    if($scope.pannumber !== ""){
      var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
      if(!regpan.test($scope.pannumber)){
        $("#loading").hide();
        service1.showMessage("error", "Please enter valid PAN number.");
        return;
      }
    }
    var dataObj = {
      "partyname":$scope.partyname,
      "address":$scope.address,
      "email_id":$scope.useremail,
      "ofcnumber":$scope.ofcnumber,
      "othernumber":$scope.othernumber,
      "pannumber":$scope.pannumber   
    };

    $http({
      method: 'POST',
      url: url.createcompanies,
      data: dataObj
    }).then(function successCallback(response) {
        //console.log(response);  
         service1.showMessage(response.data.status, response.data.message);
         $("#loading").hide();
         if(response.data.status === "success"){
          $state.reload(); 
         }
      }, function errorCallback(response) {
          //console.log(response);
          service1.showMessage(response.data.status, response.data.message);
          $("#loading").hide();
      });
  };


  //edit customer/company
  $scope.editTabClick = function(){
    $("#loading").show();
    //call to fetch company list
    /*var allrequiredData = service1.fetchData();
    allrequiredData.then(function(allData){
      $scope.companyData = allData.companyData;  
      $("#loading").hide();  
    });*/
    $http({url: url.getAllCompanies,method: 'GET'}).then(function(response) {
      console.log(response);
      if(response.data.status === 'success'){
        $scope.companyData = response.data.list;
        $("#loading").hide();
      } else {
        $scope.companyData = new Array();
        $("#loading").hide();
      }
    })
  }
  $scope.editExisting = function(singleCompany){
    $scope.edit_id = singleCompany.id;
    $scope.edit_partyname = singleCompany.party_name;
    $scope.edit_address = singleCompany.address;
    $scope.edit_useremail = singleCompany.email_id;
    $scope.edit_ofcnumber = singleCompany.office_number;
    $scope.edit_othernumber = singleCompany.other_number;
    $scope.edit_pannumber = singleCompany.PAN_number;
    $("#editCompanyModal").modal("show");
  }

  $scope.editCustomer = function(){
    $("#loading").show();
    if($scope.edit_pannumber !== ""){
      var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
      if(!regpan.test($scope.edit_pannumber)){
        $("#loading").hide();
        service1.showMessage("error", "Please enter valid PAN number.");
        return;
      }
    }
    var dataObj = {
      "id": $scope.edit_id,
      "partyname":$scope.edit_partyname,
      "address":$scope.edit_address,
      "email_id":$scope.edit_useremail,
      "ofcnumber":$scope.edit_ofcnumber,
      "othernumber":$scope.edit_othernumber,
      "pannumber":$scope.edit_pannumber   
    };

    $http({
      method: 'POST',
      url: url.updatecompany,
      data: dataObj
    }).then(function successCallback(response) {
        //console.log(response);  
         service1.showMessage(response.data.status, response.data.message);
         $("#loading").hide();
         if(response.data.status === "success"){
          $("#editCompanyModal").modal("hide");
          $scope.editTabClick(); 
         }
      }, function errorCallback(response) {
          //console.log(response);
          service1.showMessage(response.data.status, response.data.message);
          $("#loading").hide();
      });
  }
});
/*ADD OPERATION END*/

/*Custom Directive*/
mainApp.directive('customSearch', function(){
  return {
    templateUrl:"app/template/search-fields.html"
  };
});
//register salesperson and admin form directive
mainApp.directive('registrationForm', function(){
  return {
    templateUrl:"app/template/registration-form.html"
  }
});
//edit registration directive
mainApp.directive('editRegistration', function(){
  return {
    templateUrl:"app/template/edit-registration.html"
  }
});
//edit company directive
mainApp.directive('editCompany', function(){
  return {
    templateUrl:"app/template/edit-company.html"
  };
});

mainApp.controller('siteAddController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state, sites) {
  $("#loading").show();
  service1.checkSession();

  $scope.sites = sites;
  $scope.editActive = false;

  $scope.editExisting = function(val){
    $scope.site_id = val.id;
    $scope.site_code = val.site_code;
    $scope.location_name = val.location_name;
    $scope.size = val.size;
    $scope.cart_rate = val.cart_rate;
    $scope.min_cart_rate = val.min_cart_rate;
    $scope.landmark = val.landmark;
    $scope.editActive = true;
  };

  $scope.cancelClicked = function(){
    $state.reload();
  };


  $scope.addSites = function(){
    $("#loading").show();

    if($scope.editActive){
      //edit form 
      var dataObj = {
      'site_id': $scope.site_id,
      'site_code': $scope.site_code,
      'location_name': $scope.location_name,
      'size': $scope.size,
      'cart_rate': $scope.cart_rate,
      'min_cart_rate': $scope.min_cart_rate,
      'landmark': $scope.landmark
    }
    $http({
      method: 'POST',
      url: url.updatesite,
      data: dataObj
    }).then(function successCallback(response) {
        //console.log(response);  
         service1.showMessage(response.data.status, response.data.message);
         $("#loading").hide();
         $state.reload();
      }, function errorCallback(response) {
          //console.log(response);
          service1.showMessage(response.data.status, response.data.message);
          $("#loading").hide();
      });

    }else{
      var dataObj = {
      'site_code': $scope.site_code,
      'location_name': $scope.location_name,
      'size': $scope.size,
      'cart_rate': $scope.cart_rate,
      'min_cart_rate': $scope.min_cart_rate,
      'landmark': $scope.landmark
    }
    $http({
      method: 'POST',
      url: url.createsite,
      data: dataObj
    }).then(function successCallback(response) {
        //console.log(response);  
         service1.showMessage(response.data.status, response.data.message);
         $("#loading").hide();
         if(response.data.status === "success"){
          $state.reload(); 
         }
      }, function errorCallback(response) {
          //console.log(response);
          service1.showMessage(response.data.status, response.data.message);
          $("#loading").hide();
      });
    }
  };
  $("#loading").hide();
});

//Contact person add controller
mainApp.controller('cpAddController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state, companies) {
  service1.checkSession();
  $scope.companies = companies;
  $scope.isCompanySelected = false;

  $scope.companyConfig = {
    maxItems: 1,
    valueField: 'id',
    labelField: 'party_name',
    searchField: 'party_name'
    };

  $scope.createContactPerson = function(){
    var dataObj = {
      'name': $scope.cp_full_name,
      'company_id': $scope.selectedCompany,
      'contact_number': $scope.cp_contact_number,
      'email_id': $scope.cp_email_id
    } 

    $http({
      method: 'POST',
      url: url.createcontactperson,
      data: dataObj
    }).then(function successCallback(response) {
        //console.log(response);  
         service1.showMessage(response.data.status, response.data.message);
         if(response.data.status === "success"){
          $state.reload(); 
         }
      }, function errorCallback(response) {
          //console.log(response);
          service1.showMessage(response.data.status, response.data.message);
      });
  };
//edit existing
  $scope.companyConfigEditTab = {
      maxItems: 1,
      valueField: 'id',
      labelField: 'party_name',
      searchField: 'party_name',
      onChange: function(value){
        //load contact person list and active dynamic section immediatelly if responce is not empty
        //console.log('onChange', value);
        $scope.isCompanySelected = false;
        $scope.refreshCPlist(value);
      }
    };

  //refresh selected company's cp list
  $scope.refreshCPlist = function(company_id) {
    $("#loading").show();
    $http.get(url.getAllcp+company_id)
    .then(function(response){
      //console.log(response.data.list);
      //reset cp_order
      var cp_list = response.data.list;
      if(cp_list.length>0){
        $scope.contactPersons = response.data.list;
        $scope.isCompanySelected = true;
        console.log(response);
      }else{
        alert("No contact person record found under selected company. Please add contact person.");
      }          
      $("#loading").hide();
    });
  }

  $scope.editExisting = function(cp) {
    console.log("Edit");
    console.log(cp);
    $scope.edit_cp_id = cp.id;
    $scope.edit_cp_name = cp.name;
    $scope.edit_cp_email = cp.email;
    $scope.edit_cp_contact_number = cp.contact_number;
    $("#editCpModal").modal('show');
  };

  $scope.editContactPerson = function() {
    $("#loading").show();
    console.log($scope.edit_cp_id);
    var dataObj = {
      companyid: $scope.selectedCompanyForCP,
      id: $scope.edit_cp_id,
      name: $scope.edit_cp_name,
      email_id: $scope.edit_cp_email,
      contact_number: $scope.edit_cp_contact_number
    };

    $http({
      method: 'POST',
      url: url.updateContactPerson,
      data: dataObj
    }).then(function(response) {
        console.log(response);  
        $("#loading").hide();
        service1.showMessage(response.data.status, response.data.message);         
        if(response.data.status === "success"){
          $scope.refreshCPlist($scope.selectedCompanyForCP);
          $("#editCpModal").modal('hide');
        }
      });
  };

});

mainApp.controller('taxAddController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state, taxes) {
  service1.checkSession();
  $("#loading").show(); 
  //received all tax list from db
  $scope.tax_list = taxes;
  $scope.editActive = false;

  $scope.editExisting = function(val){
    $("#tax_status").val(val.status);
    $scope.tax_id = val.id;
    $scope.tax_type = val.tax_type;
    $scope.description = val.description;
    $scope.amount_in_percent = val.amount_in_percent;
    $scope.tax_status = val.status;
    $scope.editActive = true;
  };

  //cancel clicked
  $scope.cancelClicked = function(){
    $state.reload();
  };

  console.log(taxes);
  //add new tax
  $scope.addTax  =function(){
    var tax_status = $("#tax_status").val();
    console.log(tax_status);
    var dataObj = {
      tax_id:$scope.tax_id,
      tax_type:$scope.tax_type,
      description:$scope.description,
      tax_status:tax_status,
      amount_in_percent:$scope.amount_in_percent
    };

    var selected_url;
    if($scope.editActive){
      selected_url = url.updatetax;
    }else{
      selected_url = url.createtax;
    }

    $http({
      method: 'POST',
      url: selected_url,
      data: dataObj
    }).then(function successCallback(response) {
        //console.log(response);  
        $("#loading").hide(); 
         service1.showMessage(response.data.status, response.data.message);
         if(response.data.status === "success"){
          $state.reload(); 
         }         
      }, function errorCallback(response) {
          //console.log(response);
          $("#loading").hide(); 
          service1.showMessage(response.data.status, response.data.message);
      });
  };
  $("#loading").hide(); 
});


//order form controller
mainApp.controller('orderController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state, customers, salesPersons, sites) {
  service1.checkSession();

  var allrequiredData = service1.fetchData();
  allrequiredData.then(function(allData){
    $scope.companyData = allData.companyData;
    $scope.ordersData = allData.ordersData;
    $scope.siteData = allData.siteData;
  });


  $("#loading").show();
  //console.log(sites);
  $scope.customers = customers;
  $scope.salespersons = salesPersons;
  $scope.sites = sites;
  $scope.totalDays = 0;
  $scope.date_selected = false;
  $scope.spId = "";
  $scope.isCompanySelected = false;
  $scope.cpList = [];

  //dynamic form field array
  $scope.cp_order = { 
    fields: [
      {
        'cp_id': '',
        'site_id': '',
        'from_to_date':'',
        'startDate': '',
        'endDate': '',
        'totalDays': '',
        'inst_charges': 0,
        'cart_rate': 0,
        'final_amount':0,
        'od':'',
        'order_date':'',
        'order_status':''
      }
    ]};

  //to render order status model
  $scope.od_status = [{name:"Pending", value:0},{name:"Live", value:1},{name:"Over", value:2}];
 
  //Dynamically create Contact person selector depend on selected company
  $scope.customerConfig = {
    maxItems: 1,
    valueField: 'id',
    labelField: 'party_name',
    searchField: 'party_name',
    onChange: function(value){
      //load contact person list and active dynamic section immediatelly if responce is not empty
      //console.log('onChange', value);
      $("#loading").show();
      $http.get(url.getAllcp+value)
        .then(function(response){
          //console.log(response.data.list);
          //reset cp_order
          $scope.$emit('resetDynamicForm');
          var cp_list = response.data.list;
          if(cp_list.length>0){
            $scope.contactPersons = response.data.list;
            $scope.contactPersonsConfig = {
              maxItems: 1,
              valueField: 'id',
              labelField: 'name',
              searchField: 'name'
            }
            $scope.isCompanySelected = true;
          }else{
            alert("No contact person record found under selected company. Please add contact person.");
            $scope.isCompanySelected = false;
          }          
          
          $("#loading").hide();
        });
    }
    };

  //Show Site listing    
  $scope.siteConfig = {
    maxItems: 1,
    valueField: 'id',
    labelField: 'location_name',
    searchField: 'location_name'
    };  

  $scope.spConfig = {
    maxItems: 1,
    valueField: 'id',
    labelField: 'fullname',
    searchField: 'fullname'
    };

//check for number
function isANumber( n ) {
    var numStr = /^-?(\d+\.?\d*)$|(\d*\.?\d+)$/;
    return numStr.test( n.toString() );
}

//all about adding dynamic form fields
  $scope.addFormField = function() {
    var template_arr = {
        'cp_id': '',
        'site_id': '',
        'from_to_date':'',
        'startDate': '',
        'endDate': '',
        'totalDays': '',
        'inst_charges': 0,
        'cart_rate': 0,
        'final_amount':0,
        'od':'',
        'order_date':'',
        'order_status':''
      }
    //check just created dynamic fom's fields are filled or not.
    var last_element_index = $scope.cp_order.fields.length-1;
    var last_arr_element = $scope.cp_order.fields[last_element_index];
    //check is set to false for first
    var isFieldEmpty = false;

    for (var key in last_arr_element) {      
      if (last_arr_element.hasOwnProperty(key)) {
        //console.log(key + " -> " + last_arr_element[key]);
        if(last_arr_element[key] === ''){
          isFieldEmpty = true;
        }
      }
    }
    var ck1 = isANumber(last_arr_element['inst_charges']);
    var ck2 = isANumber(last_arr_element['cart_rate']);
    var ck3 = isANumber(last_arr_element['final_amount']);
    if(ck1 && ck2 && ck3){
      if(!isFieldEmpty){
      $scope.cp_order.fields.push(template_arr);  
      }else{
        alert("All Fields are required before adding new site order.")
      }
    }else{
      alert("Please enter correct cart rate/installation charges if no installation charges then write 0 in text field.")
    }    
  }


  $scope.removeChoice = function (z) {
    //var lastItem = $scope.choiceSet.choices.length - 1;
    if($scope.cp_order.fields.length === 1){
      alert("atleast one order should be placed");
    }else{
      $scope.cp_order.fields.splice(z,1);
    }    
  };

  //date range picker init
  $scope.datePicker = {};
  $scope.datePicker.dateRange = {startDate: null, endDate: null};

  //Set other fields values after selecting dateRange
  $scope.setValueToSE = function(index){
    var sDate = $scope.cp_order.fields[index].from_to_date.startDate;
    var eDate = $scope.cp_order.fields[index].from_to_date.endDate;

    if(sDate && eDate){        
      var totalDays = eDate.diff(sDate,'days');
      $scope.cp_order.fields[index].startDate = sDate.format('YYYY-MM-DD');
      $scope.cp_order.fields[index].endDate = eDate.format('YYYY-MM-DD');
      $scope.cp_order.fields[index].totalDays = totalDays+1;
      //calculate final amount depend on no. of days
      $scope.setFinalAmount(index);
    }
  }


  $scope.datePicker.od = moment();
  //set value to order Date
  $scope.setValueToOD = function(index){
    if($scope.cp_order.fields[index].od){
     $scope.cp_order.fields[index].order_date = $scope.cp_order.fields[index].od.format('YYYY-MM-DD');
    }
  }

  //set final amount depend on cart_rate, installation charges and no. of days
  $scope.setFinalAmount = function(index){
    var cart_rate = parseFloat($scope.cp_order.fields[index].cart_rate);
    var inst_charges = parseFloat($scope.cp_order.fields[index].inst_charges);
    var totalDays = parseFloat($scope.cp_order.fields[index].totalDays);
    if(inst_charges === "" || inst_charges === 0){
      inst_charges = 0;
    }
    if(cart_rate && totalDays){
      var asPerCartRate = (cart_rate*totalDays)/30;
      $scope.cp_order.fields[index].final_amount = asPerCartRate+inst_charges;
    }else{
      $scope.cp_order.fields[index].final_amount = 0;
    }
  };

  //Set cart rate depend on site selected
  $scope.setCartRateValue = function(index){
    var currentIndexSite = $scope.cp_order.fields[index].site_id;
    var keepGoing = true;
    angular.forEach(sites, function(value) {
      if(keepGoing){
        if(value.id === currentIndexSite){
          $scope.cp_order.fields[index].cart_rate = value.cart_rate;
          keepGoing = false;
        }
      }
    });
    //calculate final amount depend on cart rate
    $scope.setFinalAmount(index);
  };

  //Now consider ONLY sales person is placing order
  $scope.selectedSP = $rootScope.id;

  $("#loading").hide();

  $scope.placeOrder = function(){
    $("#loading").show();

    if($scope.selectedCustomer === undefined) { 
      service1.showMessage("error", "Select Customer"); 
      $("#loading").hide(); 
      return;
    }

    var dataObj = {
      customer_id: $scope.selectedCustomer,
      sales_person_id: $scope.selectedSP,
      orders:$scope.cp_order
    }

    $http({
      method: 'POST',
      url: url.createorder,
      data: dataObj
    }).then(function successCallback(response) {
        //console.log(response);  
        $("#loading").hide(); 
         service1.showMessage(response.data.status, response.data.message);
         if(response.data.status === "success"){
          $state.reload(); 
         }         
      }, function errorCallback(response) {
          //console.log(response);
          $("#loading").hide(); 
          service1.showMessage(response.data.status, response.data.message);
      });
  };

$scope.$on('resetDynamicForm', function(){
  $scope.cp_order = { 
  fields: [
    {
      'cp_id': '',
      'site_id': '',
      'from_to_date':'',
      'startDate': '',
      'endDate': '',
      'totalDays': '',
      'inst_charges': 0,
      'cart_rate': 0,
      'final_amount': 0,
      'od':'',
      'order_date':'',
      'order_status':''
    }
  ]};

});

});


// datatables controller

mainApp.controller('RowSelectCtrl', function($compile, $scope, $resource, service1, DTOptionsBuilder, DTColumnBuilder, $q, $http){
  var vm = this;
  vm.selected = {};
  vm.selectAll = false;
  vm.toggleAll = toggleAll;
  vm.toggleOne = toggleOne;
  $scope.company_id = [];
  $scope.activePO = false;

  var titleHtml = '<input type="checkbox" ng-model="showCase.selectAll" ng-click="showCase.toggleAll(showCase.selectAll, showCase.selected)">';

    vm.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
            return $resource(url.getpendingpoorder).query().$promise;
        }).withOption('createdRow', function(row, data, dataIndex) {
          // Recompiling so we can bind Angular directive to the DT
          $compile(angular.element(row).contents())($scope);
      })
      .withOption('headerCallback', function(header) {
          if (!vm.headerCompiled) {
              // Use this headerCompiled field to only compile header once
              vm.headerCompiled = true;
              $compile(angular.element(header).contents())($scope);
          }
      })
      .withPaginationType('full_numbers');// Active Responsive plugin
  vm.dtColumns = [
      DTColumnBuilder.newColumn(null).withTitle(titleHtml).notSortable()
      .renderWith(function(data, type, full, meta) {
          vm.selected[full.orderid] = false;
          return '<input type="checkbox" ng-model="showCase.selected[\'' + data.orderid + '\']" ng-click="showCase.toggleOne(showCase.selected)">';
      }),
          
      DTColumnBuilder.newColumn('party_name').withTitle('Party Name'),
      DTColumnBuilder.newColumn('cp_name').withTitle('Contact Person'),
      DTColumnBuilder.newColumn('location_name').withTitle('Location'),
      DTColumnBuilder.newColumn('from_date').withTitle('From date'),
      DTColumnBuilder.newColumn('to_date').withTitle('To date'),
      DTColumnBuilder.newColumn('order_amount').withTitle('Amount'),
      DTColumnBuilder.newColumn('salesperson').withTitle('Sales person').notVisible(),
      DTColumnBuilder.newColumn('site_code').withTitle('Site Code').notVisible(),
      DTColumnBuilder.newColumn('orderid').withTitle('orderid').notVisible(),
      DTColumnBuilder.newColumn('customerid').withTitle('customerid').notVisible(),

  ];
  vm.dtInstance = {};

//this function will call on select all items in table
  function toggleAll (selectAll, selectedItems) {      
    for (var id in selectedItems) {
      if (selectedItems.hasOwnProperty(id)) {
          selectedItems[id] = selectAll;
      }
    }
    $scope.activePO = $scope.isChecked();
  }

  //individual select function
  function toggleOne (selectedItems) {
    $scope.activePO = $scope.isChecked();
      for (var id in selectedItems) {
        if (selectedItems.hasOwnProperty(id)) {
          if(!selectedItems[id]) {
              vm.selectAll = false;
              return;
          }
        }
      }
    vm.selectAll = true;
  }

  $scope.setDataToParent = function(){
    var temp_order = [];
    $scope.company_id = [];
    var selectedItems = vm.selected;
    var mainData = $scope.$parent.pending_po_order;
    for (var id in selectedItems) {
      if (selectedItems.hasOwnProperty(id)) {
          if(selectedItems[id]) {
              temp_order.push(id);
              for(var k in mainData){
                if(mainData[k].orderid === id){//check for company id
                  if($scope.company_id.indexOf(mainData[k].customerid) === -1) {
                    $scope.company_id.push(mainData[k].customerid);
                  }
                }                  
              }
          }
      }
    }
    if($scope.company_id.length>1){
      service1.showMessage("error", "You can not upload PO for multiple company at a time. Please select Orders from same company only.");
      $("#poUploadModal").modal('hide');
    }else{
      $('#poUploadModal').modal('show');
      $scope.$parent.selectedOrders = temp_order;
      $scope.$parent.selectedCompany = $scope.company_id[0];
      console.log($scope.$parent.selectedOrders);
      console.log($scope.company_id);
    }
    
  };

$scope.isChecked = function(){
  var selectedItems = vm.selected;
  for (var id in selectedItems) {
    if (selectedItems.hasOwnProperty(id)) {
        if(selectedItems[id]) {
            return true;
        }
    }
  }
}

});

/*mainApp.controller('viewOrderCtrl', function($scope, $resource, $compile, DTOptionsBuilder, DTColumnBuilder, $http, $q) {
    var vm = this;
    vm.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
            return $resource(url.getAllOrders).query().$promise;
        }).withOption('createdRow', function(row, data, dataIndex) {
          // Recompiling so we can bind Angular directive to the DT
          $compile(angular.element(row).contents())($scope);
      })
      .withOption('headerCallback', function(header) {
          if (!vm.headerCompiled) {
              // Use this headerCompiled field to only compile header once
              vm.headerCompiled = true;
              $compile(angular.element(header).contents())($scope);
          }
      })
      .withPaginationType('full_numbers');

    vm.dtColumns = [
        DTColumnBuilder.newColumn('orderid').withTitle('ID'),
        DTColumnBuilder.newColumn('customer_name').withTitle('Company Name'),
        DTColumnBuilder.newColumn('spfullname').withTitle('Sales Person'),
        DTColumnBuilder.newColumn(null).withTitle("Orders").notSortable()
      .renderWith(function(data, type, full, meta) {
        return '<button type="button" class="btn btn-info btn-sm" data-toggle="modal" ng-click="showCase.showOrderDetails(\''+data.po_id+'\')">Orders</button>';
      }),
        DTColumnBuilder.newColumn(null).withTitle("Scan Copy").notSortable()
      .renderWith(function(data, type, full, meta) {
          if(data.img_url === ""){
            return 'Not available';
          }else{
            return '<a href="'+data.img_url+'" class="btn btn-warning btn-sm" target="_blank">View</a>';
          }          
      })
    ];
    vm.dtInstance = {};
});*/

mainApp.controller('viewOrderCtrl', function($scope, $resource, service1) {
    var vm = this;    
    $resource(url.getAllOrders).query().$promise.then(function(orders) {
        vm.allOrders = orders;
        console.log(vm.allOrders);
    });
});

mainApp.controller('WithPromiseCtrl', function($scope, $resource, $compile, DTOptionsBuilder, DTColumnBuilder, $http, $q) {
    var vm = this;
    vm.current_orders = [];
    vm.po_list = [];
    vm.showOrderDetails = showOrderDetails;
    vm.generatePOBill = generatePOBill;
    vm.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
            return $resource(url.getAllReceivedPO).query().$promise;
        }).withOption('createdRow', function(row, data, dataIndex) {
          // Recompiling so we can bind Angular directive to the DT
          $compile(angular.element(row).contents())($scope);
      })
      .withOption('headerCallback', function(header) {
          if (!vm.headerCompiled) {
              // Use this headerCompiled field to only compile header once
              vm.headerCompiled = true;
              $compile(angular.element(header).contents())($scope);
          }
      })
      .withPaginationType('full_numbers');

    vm.dtColumns = [
        DTColumnBuilder.newColumn('po_id').withTitle('ID'),
        DTColumnBuilder.newColumn('company_name').withTitle('Company Name'),
        DTColumnBuilder.newColumn('po_amount').withTitle('Amount'),
        DTColumnBuilder.newColumn(null).withTitle("Orders").notSortable().renderWith(function(data, type, full, meta) {
        return '<button type="button" class="btn btn-info btn-sm" data-toggle="modal" ng-click="showCase.showOrderDetails(\''+data.po_id+'\')">Orders</button>';
      }),
        DTColumnBuilder.newColumn(null).withTitle("Print Bill").notSortable().renderWith(function(data, type, full, meta) {
        return '<button type="button" class="btn btn-success btn-sm"  ng-click="showCase.generatePOBill(\''+data.po_id+'\')">Generate</button>';
      }),
      
        DTColumnBuilder.newColumn(null).withTitle("Scan Copy").notSortable()
      .renderWith(function(data, type, full, meta) {
          if(data.img_url === ""){
            return 'Not available';
          }else{
            return '<a href="'+data.img_url+'" class="btn btn-warning btn-sm" target="_blank">View</a>';
          }          
      })

    ];
    vm.dtInstance = {};

    function showOrderDetails (po_id){
      var list = $scope.$parent.receivedPO;
      for(var i in list){
        if(list[i]['po_id'] === po_id){
          vm.current_orders = list[i].orders;                          
        }
      }
      $('#orderDetailsModal').modal('show');      
    }

    function generatePOBill(po_id){
      console.log("bill generated");
      var list = $scope.$parent.receivedPO;
      for(var i in list){
        if(list[i]['po_id'] === po_id){
          vm.current_orders = list[i].orders;
          console.log(vm.current_orders);
        }
      }
    }
});

//PO Controller
mainApp.controller('poController', function ($scope, $stateParams, $http, service1, $location, $cookieStore, $rootScope, $state, pending_po_order, customers, receivedPO, Upload, $timeout) {
  service1.checkSession();
  //received from resolve function
  $scope.pending_po_order = pending_po_order;
  $scope.receivedPO = receivedPO;
  $scope.customers = customers;
  //params for upload po
  $scope.selectedCompany = "";
  $scope.selectedOrders = [];
  $scope.img_url = "";

  
  //PO image Upload
    $scope.uploadFiles = function(file, errFiles) {
      $scope.f = file;
      $scope.errFile = errFiles && errFiles[0];
      if (file) {
        file.upload = Upload.upload({
          url: url.poUpload,
          data: {fileToUpload: file}
        });

        file.upload.then(function (response) {
          $timeout(function () {
            file.result = response.data.message;
            if(response.data.status === "success"){
              $("#uploaded_file_path").val(response.data.file_path);
              $scope.img_url = response.data.file_path;
            }else{
              console.log(response.data);
              service1.showMessage("error", response.data);
            }           
          });
        }, function (response) {
            if(response.data.status === "success"){
              $("#uploaded_file_path").val(response.data.file_path);
              $scope.img_url = response.data.file_path;
            }else{
              console.log(response.data);
              service1.showMessage("error", response.data);
            }

        }, function (evt) {
          file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
      }   
    }

//call on save new pos
    $scope.addPO = function(){
      $("#loading").show();      
      $("#poUploadModal").modal('hide');
      var dataObj = {
        'order_list': $scope.selectedOrders,
        'company_id': $scope.selectedCompany,
        'administrator_id': $rootScope.id,
        'po_number': $scope.po_number,
        'po_amount': $scope.po_amount,
        'img_url': $scope.img_url
      };
      $http({
        method: 'POST',
        url: url.createpo,
        data: dataObj
      }).then(function successCallback(response) {
           $("#loading").hide();
           service1.showMessage(response.data.status, response.data.message);
           if(response.data.status === "success"){
            $state.reload(); 
           }
        }, function errorCallback(response) {
            console.log(response);
            service1.showMessage(response.data.status, response.data.message);
            $("#loading").hide();
        });
    }
});