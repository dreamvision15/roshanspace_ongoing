<?php
header('Access-Control-Allow-Origin: *');
require '../vendor/autoload.php';
require_once '../include/DbOperation.php';

$app = new Slim\App();

//Create New MasterAdmin
$app->post('/create/master_admin', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->createMasterAdmin($request);
});

//Create New Administrator
$app->post('/create/administrator', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->createAdministrator($request);
});

//update Administrator
$app->post('/update/administrator', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->updateAdministrator($request);
});

//All AdminList
$app->get('/admin_list', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getAllAdmins($request);
});



//Create New SalesPerson
$app->post('/create/sales_person', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->createSalesPerson($request);
});

//update sp
$app->post('/update/salesperson', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->updateSalesPerson($request);
});

//Create New Customer
$app->post('/companies/create', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->createCompany($request);
});

//update sp
$app->post('/update/company', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->updateCompany($request);
});

//Create New Site
$app->post('/sites/create', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->createSite($request);
});


//Update site
$app->post('/sites/update', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->updateSite($request);
});

//Login handler
$app->post('/login', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->loginToRS($request);
});

//All Customers handler
$app->get('/companies', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getAllCompanies($request);
});

//All SalesPerson handler
$app->get('/salespersons', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getAllSalespersons($request);
});

//All Orders handler
$app->get('/orders', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getAllOrders($request);
});

//All Sites handler
$app->get('/sites', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getAllSites($request);
});
//save orders 
$app->post('/orders/create', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->saveOrders($request);
});

//udpate order
$app->post('/orders/update', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->updateOrder($request);
});

//save po 
$app->post('/po/create', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->savePO($request);
});

//save po 
$app->post('/contactpersons/create', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->saveContactPerson($request);
});

// fetch all orders with no po
$app->get('/orders/pendingpo', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getPendingPOorders($request);
});

// fetch all sites available for selected date
$app->get('/sites/available/{date}', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getAvailableSites($request);
});
// fetch all sites booked for selected date
$app->get('/sites/booked/{date}', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getBookedSites($request);
});
/*$app->post('/createnew', function ($request, $response, $args) {
    getParam('username');

});
*/
// fetch all contact persons by company
$app->get('/contactpersons/{companyid}', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getContactPersons($request);
});

//edit contact person as per selected company
$app->post('/contactpersons/update', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->updateContactPersons($request);
});

//fetch pending po by compant id
$app->get('/orders/companypendingpo/{companyid}', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getCompanyPendingPO($request);
});

//fetch received po by company id
$app->get('/po/received', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getReceivedPOList($request);
});

//fetch received po by company id
$app->get('/company/status/{company_id}', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getCompanyStatus($request);
});

//fetch received po by company id
$app->get('/company/all/status', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getAllCompanyStatus($request);
});

//fetch tax info
$app->get('/tax/view', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->getAllTaxInfo($request);
});
$app->post('/tax/create', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->saveTaxInfo($request);
});
$app->post('/tax/update', function ($request, $response, $args) {
    $db = new DbOperation();
    $res = $db->updateTaxInfo($request);
});

$app->run();	
