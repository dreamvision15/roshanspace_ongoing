<?php

class DbOperation
{
    //Database connection link
    private $con;

    //Class constructor
    function __construct()
    {
        //Getting the DbConnect.php file
        require_once dirname(__FILE__) . '/DbConnect.php';

        //Creating a DbConnect object to connect to the database
        $db = new DbConnect();

        //Initializing our connection link of this class
        //by calling the method connect of DbConnect class
        $this->con = $db->connect();
    }


    //method to create Master Admin
    public function createMasterAdmin($request){
        $first_name = $request->getParam('first_name');
        $last_name = $request->getParam('last_name');
        $email_id = $request->getParam('email_id');
        $password = $request->getParam('password');         
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);
        $contact_number = $request->getParam('contact_number'); // by key
        $address = $request->getParam('address');
        $created_at = date("Y-m-d H:i:s"); 

        $data = $request->getParams(); // all data from query string or post body
        $responseData = array();
        if($first_name == "" || $last_name == "" || $contact_number == "" || $address == "" || $email_id == "" || $password == ""){      
            $responseData['status'] = "error";
            $responseData['message'] = "Some fields are missing";
            echo json_encode($responseData);
        }else{
            if(!$this->isExists($email_id, 'master_admin')) {            
                $id = uniqid('ma_');
                $query = "INSERT INTO master_admin(id, first_name, last_name, email_id, password, contact_number, address, created_at) VALUES ('$id', '$first_name', '$last_name', '$email_id', '$hashed_password', '$contact_number', '$address', '$created_at')";
                //Crating an statement
                $stmt = $this->con->prepare($query);
                //Executing the statment
                $result = $stmt->execute();
                //Closing the statment
                $this->con = null;
                if ($result) {
                //Insert success
                    $responseData['status'] = "success";
                    $responseData['message'] = "Success! account with Email id: ".$email_id." created";
                    echo json_encode($responseData);
                } else {
                //Insert fail
                    $responseData['status'] = "error";
                    $responseData['message'] = "Error! Something went wrong";
                    echo json_encode($responseData);
                }
            }else{
                $responseData['status'] = "error";
                $responseData['message'] = "Error! user already exist.";
                echo json_encode($responseData);
            }        
        }
    }

    //method to create Administrator
    public function createAdministrator($request){
        $first_name = $request->getParam('first_name');
        $last_name = $request->getParam('last_name');
        $email_id = $request->getParam('email_id');
        $password = $request->getParam('password');         
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);
        $contact_number = $request->getParam('contact_number'); // by key
        $address = $request->getParam('address');
        $status_val = $request->getParam('status');        
        $created_at = date("Y-m-d H:i:s"); 

        $data = $request->getParams(); // all data from query string or post body
        $responseData = array();
        if($first_name == "" || $last_name == "" || $contact_number == "" || $address == "" || $email_id == "" || $password == ""){      
            $responseData['status'] = "error";
            $responseData['message'] = "Some fields are missing";
            echo json_encode($responseData);
        }else{
            if(!$this->isExists($email_id,'administrator')) {            
                $id = uniqid('ad_');
                $query = "INSERT INTO administrator(id, first_name, last_name, email_id, password, contact_number, address, status, created_at) VALUES ('$id', '$first_name', '$last_name', '$email_id', '$hashed_password', '$contact_number', '$address', '$status_val', '$created_at')";
                //Crating an statement
                $stmt = $this->con->prepare($query);
                //Executing the statment
                $result = $stmt->execute();
                //Closing the statment
                $this->con = null;
                if ($result) {
                    //Insert success
                    $responseData['status'] = "success";
                    $responseData['message'] = "Success! account with Email id: ".$email_id." created";
                    echo json_encode($responseData);
                } else {
                    //Insert fail
                    $responseData['status'] = "error";
                    $responseData['message'] = "Error! Something went wrong";
                    echo json_encode($responseData);
                }
            }else{
                $responseData['status'] = "error";
                $responseData['message'] = "Error! user already exist.";
                echo json_encode($responseData);
            }        
        }
    }

//update administrator details
    public function updateAdministrator($request){
        $id = $request->getParam('id');
        $first_name = $request->getParam('first_name');
        $last_name = $request->getParam('last_name');
        $email_id = $request->getParam('email_id');
        $contact_number = $request->getParam('contact_number'); // by key
        $address = $request->getParam('address');
        $status_val = $request->getParam('status');

        $responseData = array();
        $query = "SELECT id FROM administrator WHERE id = '$id' ";
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(count($result)){
            $query = "UPDATE administrator ";
            $query.= "SET first_name='$first_name', last_name='$last_name', contact_number='$contact_number', address='$address', status='$status_val'";

            //check password is also changed or not
            $change_password_flag = $request->getParam('change_password');        
            if($change_password_flag){
                $password = $request->getParam('new_password');         
                $hashed_password = password_hash($password, PASSWORD_DEFAULT); 
                $query.=", password='$hashed_password'";   
            }
            $query.=" WHERE id='$id'";
            //echo $query;
            //Crating an statement
            $stmt = $this->con->prepare($query);
            //Executing the statment
            $result = $stmt->execute();
            //Closing the statment
            $this->con = null;
            if ($result) {
            //Insert success
                $responseData['status'] = "success";
                $responseData['message'] = "Success! Administrator details updated Successfully.";
            } else {
            //Insert fail
                $responseData['status'] = "error";
                $responseData['message'] = "Error! Something went wrong Please refresh and try again.";
            }
        } else {
            $responseData['status'] = "error";
            $responseData['message'] = "Error! Please check administrator details and try again.";
            
        }
    echo json_encode($responseData);

    } 



    public function getAllAdmins($request){
        $query = "SELECT * FROM `administrator`";
        //Crating an statement
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $responseData = array();
        if(count($result)){
            $responseData['status'] = "success";
        }else{
            $responseData['status'] = "error";
            $responseData['message'] = "Fail to get administrators List/ administrators list not available.";
        }
        $responseData['list'] = $result;
        echo json_encode($responseData);
        //Closing the statment
        $this->con = null;
        exit();
    }


    //method to create Sales Person
    public function createSalesPerson($request){
        $first_name = $request->getParam('first_name');
        $last_name = $request->getParam('last_name');
        $email_id = $request->getParam('email_id');
        $password = $request->getParam('password');         
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);
        $contact_number = $request->getParam('contact_number'); // by key
        $address = $request->getParam('address');
        $joining_date = $request->getParam('joining_date');
        $leaving_date = $request->getParam('leaving_date');
        $created_at = date("Y-m-d H:i:s"); 

        $data = $request->getParams(); // all data from query string or post body
        $responseData = array();
        if($first_name == "" || $last_name == "" || $contact_number == "" || $address == "" || $email_id == "" || $password == ""){      
            $responseData['status'] = "error";
            $responseData['message'] = "Some fields are missing";
            echo json_encode($responseData);            
        }else{
            if(!$this->isExists($email_id,'sales_person')) {            
                $id = uniqid('sp_');
                $query = "INSERT INTO sales_person(id, first_name, last_name, email_id, password, contact_number, address, joining_date, leaving_date, created_at) VALUES ('$id', '$first_name', '$last_name', '$email_id', '$hashed_password', '$contact_number', '$address', '$joining_date', '$leaving_date', '$created_at')";
                //Crating an statement
                $stmt = $this->con->prepare($query);
                //Executing the statment
                $result = $stmt->execute();
                //Closing the statment
                $this->con = null;
                if ($result) {
                //Insert success
                    $responseData['status'] = "success";
                    $responseData['message'] = "Success! account with Email id: ".$email_id." created";
                    echo json_encode($responseData);
                } else {
                //Insert fail
                    $responseData['status'] = "error";
                    $responseData['message'] = "Error! Something went wrong";
                    echo json_encode($responseData);
                }
            }else{
                $responseData['status'] = "error";
                $responseData['message'] = "Error! user already exist.";
                echo json_encode($responseData);
            }        
        }
    }



//update salesperson details
    public function updateSalesPerson($request){
        $id = $request->getParam('id');
        $first_name = $request->getParam('first_name');
        $last_name = $request->getParam('last_name');
        $email_id = $request->getParam('email_id');
        $contact_number = $request->getParam('contact_number'); // by key
        $address = $request->getParam('address');
        $status_val = $request->getParam('status');

        $responseData = array();
        $query = "SELECT id FROM sales_person WHERE id = '$id' ";
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(count($result)){
            $query = "UPDATE sales_person ";
            $query.= "SET first_name='$first_name', last_name='$last_name', contact_number='$contact_number', address='$address', status='$status_val'";

            //check password is also changed or not
            $change_password_flag = $request->getParam('change_password');        
            if($change_password_flag){
                $password = $request->getParam('new_password');         
                $hashed_password = password_hash($password, PASSWORD_DEFAULT); 
                $query.=", password='$hashed_password'";   
            }
            $query.=" WHERE id='$id'";
            //echo $query;
            //Crating an statement
            $stmt = $this->con->prepare($query);
            //Executing the statment
            $result = $stmt->execute();
            //Closing the statment
            $this->con = null;
            if ($result) {
            //Insert success
                $responseData['status'] = "success";
                $responseData['message'] = "Success! Sales Person details updated Successfully.";
            } else {
            //Insert fail
                $responseData['status'] = "error";
                $responseData['message'] = "Error! Something went wrong Please refresh and try again.";
            }
        } else {
            $responseData['status'] = "error";
            $responseData['message'] = "Error! Please check sales person details and try again.";
            
        }
    echo json_encode($responseData);

    }



    //method to create site
    public function createSite($request){
        $site_code = $request->getParam('site_code');
        $location_name = $request->getParam('location_name');
        $size = $request->getParam('size');
        $cart_rate = $request->getParam('cart_rate');
        $min_cart_rate = $request->getParam('min_cart_rate');
        $landmark = $request->getParam('landmark');

        $query = "SELECT id FROM site_info WHERE site_code = '$site_code' ";
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(count($result)){
            $responseData['status'] = "error";
            $responseData['message'] = "Error! Site with same sitecode already exist.";
        } else {
            $id = uniqid('st_');
            $created_at = date("Y-m-d H:i:s"); 
            $query = "INSERT INTO site_info(id, site_code, location_name, size, cart_rate, min_cart_rate, landmark, created_at) VALUES ('$id', '$site_code', '$location_name', '$size', '$cart_rate', '$min_cart_rate', '$landmark','$created_at')";
            //Crating an statement
            $stmt = $this->con->prepare($query);
            //Executing the statment
            $result = $stmt->execute();
            //Closing the statment
            $this->con = null;
            if ($result) {
            //Insert success
                $responseData['status'] = "success";
                $responseData['message'] = "Success! Site added Successfully.";
            } else {
            //Insert fail
                $responseData['status'] = "error";
                $responseData['message'] = "Error! Something went wrong Please refresh and try again.";
            }
        }
        echo json_encode($responseData);
    }


    //method to update site
    public function updateSite($request){
        $site_id = $request->getParam('site_id');
        $site_code = $request->getParam('site_code');
        $location_name = $request->getParam('location_name');
        $size = $request->getParam('size');
        $cart_rate = $request->getParam('cart_rate');
        $min_cart_rate = $request->getParam('min_cart_rate');
        $landmark = $request->getParam('landmark');

        $query = "SELECT id FROM site_info WHERE id = '$site_id' ";
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(count($result)){
            $created_at = date("Y-m-d H:i:s"); 
            $query = "UPDATE site_info ";
            $query.="SET location_name='$location_name', size='$size', site_code='$site_code', cart_rate='$cart_rate', min_cart_rate='$min_cart_rate', landmark='$landmark', created_at='$created_at' ";
            $query.="WHERE id='$site_id'";
            //Crating an statement
            $stmt = $this->con->prepare($query);
            //Executing the statment
            $result = $stmt->execute();
            //Closing the statment
            $this->con = null;
            if ($result) {
            //Insert success
                $responseData['status'] = "success";
                $responseData['message'] = "Success! Site updated Successfully.";
            } else {
            //Insert fail
                $responseData['status'] = "error";
                $responseData['message'] = "Error! Something went wrong Please refresh and try again.";
            }
        } else {
            $responseData['status'] = "error";
            $responseData['message'] = "Error! Something went wrong Please refresh and try again.";
            
        }
        echo json_encode($responseData);
    }

    //method to create Company
    public function createCompany($request){
        $party_name = $request->getParam('partyname');
        $email_id = $request->getParam('email_id');
        $office_number = $request->getParam('ofcnumber');
        $other_number = $request->getParam('othernumber');
        $pan_number = $request->getParam('pannumber');
        $address = $request->getParam('address');
        $created_at = date("Y-m-d H:i:s"); 

        $data = $request->getParams(); // all data from query string or post body
        $responseData = array();
        if($party_name == "" || $email_id == "" || $office_number == "" || $address == "" || $pan_number == ""){      
            $responseData['status'] = "error";
            $responseData['message'] = "Some fields are missing";
            echo json_encode($responseData);            
        }else{
            if(!$this->isExists($email_id,'customer')) {            
                $id = uniqid('cust_');
                $query = "INSERT INTO companies(id, party_name, email_id, address, office_number, other_number, PAN_number, created_at) VALUES ('$id', '$party_name', '$email_id', '$address', '$office_number', '$other_number', '$pan_number', '$created_at')";
                //Crating an statement
                $stmt = $this->con->prepare($query);
                //Executing the statment
                $result = $stmt->execute();
                //Closing the statment
                $this->con = null;
                if ($result) {
                //Insert success
                    $responseData['status'] = "success";
                    $responseData['message'] = "Success! Company added Successfully.";
                    echo json_encode($responseData);
                } else {
                //Insert fail
                    $responseData['status'] = "error";
                    $responseData['message'] = "Error! Something went wrong Please refresh and try again.";
                    echo json_encode($responseData);
                }
            }else{
                $responseData['status'] = "error";
                $responseData['message'] = "Error! Company with same email id already exist.";
                echo json_encode($responseData);
            }        
        }
    }


    //method to create Company
    public function updateCompany($request){
        $id = $request->getParam('id');
        $party_name = $request->getParam('partyname');
        $email_id = $request->getParam('email_id');
        $office_number = $request->getParam('ofcnumber');
        $other_number = $request->getParam('othernumber');
        $pan_number = $request->getParam('pannumber');
        $address = $request->getParam('address');        

        $data = $request->getParams(); // all data from query string or post body
        $responseData = array();
        /*echo "****".$id."***";
        print_r($data);*/

        $query = "SELECT id FROM companies WHERE id = '$id' ";
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(count($result)){
            if($party_name == "" || $email_id == "" || $office_number == "" || $address == "" || $pan_number == ""){      
                $responseData['status'] = "error";
                $responseData['message'] = "Some fields are missing";
            }else{                         
                $query = "UPDATE companies ";
                $query.= "SET party_name='$party_name', email_id='$email_id', address='$address', office_number='$office_number', other_number='$other_number', pan_number='$pan_number' ";
                $query.= "WHERE id='$id'";
                //Crating an statement
                $stmt = $this->con->prepare($query);
                //Executing the statment
                $result = $stmt->execute();
                //Closing the statment
                $this->con = null;
                if ($result) {
                //update success
                    $responseData['status'] = "success";
                    $responseData['message'] = "Success! Company details updated Successfully.";
                } else {
                //update fail
                    $responseData['status'] = "error";
                    $responseData['message'] = "Error! Something went wrong Please refresh and try again.";
                }                     
            }
        }else{
            $responseData['status'] = "error";
            $responseData['message'] = "Error! Company you want to edit not present.";
        }
        echo json_encode($responseData);        
    }



    //Checking whether a student already exist
    private function isExists($email_id, $type) {
        $query = null;
        switch ($type) {
            case 'master_admin':            
                $query = "SELECT id from master_admin WHERE email_id = '$email_id'";
            break;
            case 'administrator':
                $query = "SELECT id from administrator WHERE email_id = '$email_id'";
            break;
            case 'sales_person':
                $query = "SELECT id from sales_person WHERE email_id = '$email_id'";
            break;
            case 'customer':
                $query = "SELECT id from companies WHERE email_id = '$email_id'";
            break;
        }
        //Crating an statement
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $num_rows = count($result);
        return $num_rows > 0;
    }

    //This method will generate a unique api key
    private function generateApiKey(){
        return md5(uniqid(rand(), true));
    }

    public function getAllCompanies($request){
        $query = "SELECT `id`,`party_name`,`address`,`office_number`,`other_number`,`PAN_number`,`email_id`,date(`created_at`) datecreated FROM `companies` ";
        //Crating an statement
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $responseData = array();
        if(count($result)){
            $responseData['status'] = "success";
        }else{
            $responseData['status'] = "error";
            $responseData['message'] = "Fail to get customer List/ Customers list not available.";
        }
        $responseData['list'] = $result;
        echo json_encode($responseData);
        //Closing the statment
        $this->con = null;
        exit();
    }


//locally used function to get sttus of company SHORT/EXCESS
    private function getIndividualCompanyStatus($company_id){
        $status = "";
        $ord_query = "SELECT SUM(amount) total FROM order_info WHERE customer_id = '$company_id'";
        $po_query = "SELECT SUM(po_amount) total FROM po_received WHERE company_id = '$company_id'";
        //Crating an statement
        $stmt = $this->con->prepare($ord_query);
        $stmt->execute();
        $ord_result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        //echo $po_query;
        $stmt = $this->con->prepare($po_query);
        $stmt->execute();
        $po_result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if($po_result[0]['total'] > $ord_result[0]['total']){
            $status = "excess";
        }else{
            $status = "short";
        }
        if($po_result[0]['total'] === $ord_result[0]['total']){//rqual total
            $status = "equal";        
        }
        if($ord_result[0]['total'] === null){//no order placed
            $status = "none";
        }
        return $status;
    }


//API call to get individual company status
    public function getCompanyStatus($request){
        $company_id = $request->getAttribute('company_id');
        $status = "";
        $ord_query = "SELECT SUM(amount) total FROM order_info WHERE customer_id = '$company_id'";
        $po_query = "SELECT SUM(po_amount) total FROM po_received WHERE company_id = '$company_id'";
        //Crating an statement
        $stmt = $this->con->prepare($ord_query);
        $stmt->execute();
        $ord_result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        //echo $po_query;
        $stmt = $this->con->prepare($po_query);
        $stmt->execute();
        $po_result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if($po_result[0]['total'] > $ord_result[0]['total']){
            $status = "excess";
        }else{
            $status = "short";
        }
        if($po_result[0]['total'] === $ord_result[0]['total']){//rqual total
            $status = "equal";        
        }
        if($ord_result[0]['total'] === null){//no order placed
            $status = "none";
        }
        $responseData = array();
        $responseData['order_total'] = $ord_result[0]['total'];
        $responseData['po_total'] = $po_result[0]['total'];
        $responseData['company_status'] = $status;      
        echo json_encode($responseData);
        //Closing the statment
        $this->con = null;
        exit();
    }


//API to get status of all companies
    public function getAllCompanyStatus($request){
        $companyStatusArr = array();
        $company_query = "SELECT id, party_name FROM companies";
        $stmt = $this->con->prepare($company_query);
        $stmt->execute();
        $company_result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $company_count = count($company_result);

        //re-format response
        for ($i=0; $i < $company_count; $i++) {
            $object = new stdClass();
            $company_id = $company_result[$i]['id'];
            $object->company_id = $company_result[$i]['id'];
            $object->party_name = $company_result[$i]['party_name'];
            $status = "";
            $ord_query = "SELECT SUM(amount) total FROM order_info WHERE customer_id = '$company_id'";
            $po_query = "SELECT SUM(po_amount) total FROM po_received WHERE company_id = '$company_id'";
            //Crating an statement
            $stmt = $this->con->prepare($ord_query);
            $stmt->execute();
            $ord_result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            //echo $po_query;
            $stmt = $this->con->prepare($po_query);
            $stmt->execute();
            $po_result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if($po_result[0]['total'] > $ord_result[0]['total']){
            $status = "excess";
            }else{
                $status = "short";
            }
            if($po_result[0]['total'] === $ord_result[0]['total']){//rqual total
                $status = "equal";        
            }
            if($ord_result[0]['total'] === null){//no order placed
                $status = "none";
            }
            $object->order_total = $ord_result[0]['total'];
            $object->po_total = $po_result[0]['total'];
            $object->company_status = $status;
            array_push($companyStatusArr, $object);
        }
        echo json_encode($companyStatusArr);
        //Closing the statment
        $this->con = null;
        exit();
    }

    public function getPendingPOorders($request){
        //$query = "select t1.id orderid,t3.id customerid,t3.party_name,t4.id salespersonid,CONCAT(t4.first_name,'',t4.last_name) salesperson,t5.id site_id, t5.location_name ,t5.site_code from order_info t1 inner join companies t3 on t3.id = t1.customer_id inner join sales_person t4 on t4.id = t1.sales_person_id inner join site_info t5 on t5.id = t1.site_id left join po_received t2 on t1.id = t2.order_id where t2.id is null";
        $query = "select t1.id orderid,t1.amount as order_amount,t1.from_date,t1.to_date,t3.id customerid,t3.party_name,t4.id salespersonid,CONCAT(t4.first_name,' ',t4.last_name) salesperson,t5.id site_id,t5.location_name,t5.site_code,t6.id,t6.name cp_name,t6.email cp_email from order_info t1 inner join companies t3 on t3.id = t1.customer_id inner join sales_person t4 on t4.id = t1.sales_person_id inner join site_info t5 on t5.id = t1.site_id inner join contact_persons t6 on t1.cp_id = t6.id where t1.po_id is null";
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $responseData = array();
        if(count($result)){
            $responseData['status'] = "success";
        }else{
            $responseData['status'] = "error";
            $responseData['message'] = "Fail to get Pending PO Orders/ Pending Orders not available.";
        }
        $responseData['list'] = $result;
        //echo json_encode($responseData);
        echo json_encode($result);
        //Closing the statment
        $this->con = null;
        exit();
    }


    //po by company id
    public function getCompanyPendingPO($request){
        $companyid = $request->getAttribute('companyid');
        $query = "select t1.id orderid,t1.from_date,t1.to_date,t3.id customerid,t3.party_name,t4.id salespersonid,CONCAT(t4.first_name,' ',t4.last_name) salesperson,t5.id site_id,t5.location_name,t5.site_code,t6.id,t6.name cp_name,t6.email cp_email from order_info t1 inner join companies t3 on t3.id = t1.customer_id inner join sales_person t4 on t4.id = t1.sales_person_id inner join site_info t5 on t5.id = t1.site_id inner join contact_persons t6 on t1.cp_id = t6.id where t1.po_id is null and customer_id = '$companyid'";
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $responseData = array();
        if(count($result)){
            $responseData['status'] = "success";
        }else{
            $responseData['status'] = "error";
            $responseData['message'] = "Fail to get Pending PO Orders/ Pending Orders not available.";
        }
        $responseData['list'] = $result;
        //echo json_encode($responseData);
        echo json_encode($result);
        //Closing the statment
        $this->con = null;
        exit();
    }


    //get list of received po
    public function getReceivedPOList($request){
        //custom param to form response
        $po_list = array();
        $po_id_check = array();
        $query = "SELECT t1.id as order_id, t1.amount as order_amount, t1.from_date, t1.to_date, t2.id as po_id, t2.po_number, t2.po_amount, t2.img_url, t3.id as company_id, t3.party_name, t4.name as cp_name, t5.location_name, t5.site_code, t5.size FROM order_info t1 INNER JOIN po_received t2 on t1.po_id = t2.id INNER JOIN companies t3 on t3.id = t1.customer_id INNER JOIN contact_persons t4 on t4.id = t1.cp_id INNER JOIN site_info t5 on t1.site_id = t5.id";
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);        
        $result_count = count($result);
        //re-format response
        for ($i=0; $i < $result_count; $i++) {                         
            if (!in_array($result[$i]['po_id'], $po_id_check)) {
                array_push($po_id_check, $result[$i]['po_id']);
                $object = new stdClass();
                $object->po_id = $result[$i]['po_id'];
                $object->company_name = $result[$i]['party_name'];
                $object->po_number = $result[$i]['po_number'];
                $object->po_amount = $result[$i]['po_amount'];
                $object->img_url = $result[$i]['img_url'];
                $orders_arr = array();
                for($j=0; $j < $result_count; $j++){//check for multiple orders under single po received
                 if($result[$i]['po_id'] === $result[$j]['po_id']){
                    $ord_obj = new stdClass();
                    $ord_obj->order_id = $result[$j]['order_id'];
                    $ord_obj->cp_name = $result[$j]['cp_name'];
                    $ord_obj->location_name = $result[$j]['location_name'];
                    $ord_obj->order_amount = $result[$j]['order_amount'];
                    $ord_obj->from_date = $result[$j]['from_date'];
                    $ord_obj->to_date = $result[$j]['to_date'];
                    array_push($orders_arr, $ord_obj);            
                 }
                }
                $object->orders = $orders_arr;//assign order array to orders
                array_push($po_list, $object);
            }
        }
        //print_r($po_list);
        $responseData = array();
        if(count($result)){
            $responseData['status'] = "success";
        }else{
            $responseData['status'] = "error";
            $responseData['message'] = "Fail to get Pending PO Orders/ Pending Orders not available.";
        }
        $responseData['list'] = $po_list;
        //echo json_encode($responseData);
        echo json_encode($po_list);        
        //Closing the statment
        $this->con = null;
        exit();
    }



    public function getAvailableSites($request){
        $dateSelected = $request->getAttribute('date');
        $query = "select t1.id,t1.location_name,t1.site_code,t1.landmark from site_info t1 left join order_info t2 on t2.site_id = t1.id where ( '$dateSelected' NOT between t2.from_date and t2.to_date) or ( t2.id is null)";
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $responseData = array();
        if(count($result)){
            $responseData['status'] = "success";
        }else{
            $responseData['status'] = "error";
            $responseData['message'] = "Fail to get Available Sites/ No sites available.";
        }
        $responseData['list'] = $result;
        echo json_encode($responseData);
        //Closing the statment
        $this->con = null;
        exit();
    }
    public function getBookedSites($request){
        $dateSelected = $request->getAttribute('date');
        $query = "select t1.id,t1.location_name,t1.site_code,t1.landmark from site_info t1 inner join order_info t2 on t2.site_id = t1.id where ( '$dateSelected' between t2.from_date and t2.to_date)";
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $responseData = array();
        if(count($result)){
            $responseData['status'] = "success";
        }else{
            $responseData['status'] = "error";
            $responseData['message'] = "Fail to get Booked Sites/ No sites Booked.";
        }
        $responseData['list'] = $result;
        echo json_encode($responseData);
        //Closing the statment
        $this->con = null;
        exit();
    }
    public function getContactPersons($request){
        $companyid = $request->getAttribute('companyid');
        $query = "select id,email,name,contact_number from contact_persons where company_id = '$companyid'";
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $responseData = array();
        if(count($result)){
            $responseData['status'] = "success";
        }else{
            $responseData['status'] = "error";
            $responseData['message'] = "Fail to get Contact Persons/ No Contact Persons Present.";
        }
        $responseData['list'] = $result;
        echo json_encode($responseData);
        //Closing the statment
        $this->con = null;
        exit();
    }

    public function updateContactPersons($request) {
        $companyid = $request->getAttribute('companyid');
        $cp_id = $request->getParam('id');
        $email_id = $request->getParam('email_id');
        $name = $request->getParam('name');
        $contact_number = $request->getParam('contact_number');
        $query  = "UPDATE contact_persons SET email = '$email_id', name = '$name', contact_number = '$contact_number' WHERE id = '$cp_id'";

        $stmt = $this->con->prepare($query);
        //Executing the statment
         $result = $stmt->execute();
        if ($result) {
            //Insert success
            $responseData['status'] = "success";
            $responseData['message'] = "Success! Contact Person information Updated";
        } else {
            //Insert fail
            $responseData['status'] = "error";
            $responseData['message'] = "Error! Something went wrong";
        }
        echo json_encode($responseData);
    }


    public function saveOrders($request){        
        $customer_id = $request->getParam('customer_id');
        $sales_person_id = $request->getParam('sales_person_id');
        $created_at = date("Y-m-d H:i:s");
        $orders = $request->getParam('orders');
        $order_len = count($orders['fields']);
        $query = "INSERT INTO order_info(id, customer_id, sales_person_id, cp_id, site_id, from_date, to_date, days, custom_cart_rate,order_date, installation_charges, amount, order_status , created_at) VALUES ";
        for ($i=0; $i < $order_len; $i++) { 
            $order_id = uniqid('ord_').mt_rand();            
            $singleObj = $orders['fields'][$i];
            $cp_id = $singleObj['cp_id'];
            $site_id = $singleObj['site_id'];
            $from_date = $singleObj['startDate'];
            $to_date = $singleObj['endDate'];
            $number_days = $singleObj['totalDays'];
            $custom_cart_rate = $singleObj['cart_rate'];
            $order_date = $singleObj['order_date'];
            $installation_charges = $singleObj['inst_charges'];
            $amount = $singleObj['final_amount'];
            $order_status = $singleObj['order_status'];

            $query.= "('$order_id', '$customer_id', '$sales_person_id', '$cp_id', '$site_id', '$from_date', '$to_date', '$number_days', $custom_cart_rate,'$order_date',$installation_charges,$amount,$order_status, '$created_at')";
            if($i !== ($order_len-1)){
                $query.=", ";
            }            
        }
        //echo $query;        
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $result = $stmt->execute();
        if ($result) {
            //Insert success
            $responseData['status'] = "success";
            $responseData['message'] = "Success! Order created";
        } else {
            //Insert fail
            $responseData['status'] = "error";
            $responseData['message'] = "Error! Something went wrong";
        }
        echo json_encode($responseData);
    }
    public function updateOrder($request){
        $order_id = $request->getParam('order_id');
        $customer_id = $request->getParam('customer_id');
        $sales_person_id = $request->getParam('sales_person_id');
        $site_id = $request->getParam('site_id');
        $from_date = $request->getParam('from_date');        
        $to_date = $request->getParam('to_date');
        $custom_cart_rate = $request->getParam('custom_cart_rate');
        $order_date = $request->getParam('order_date');
        $installation_charges = $request->getParam('installation_charges');
        $amount = $request->getParam('amount');
        $number_days = date_diff(date_create($to_date),date_create($from_date));
        $order_status = $request->getParam('order_status');
        $created_at = date("Y-m-d H:i:s");
        $query = "UPDATE order_info SET customer_id = '".$customer_id."', sales_person_id = '".$sales_person_id."', site_id = '".$site_id."', from_date = '".$from_date."', to_date = '".$to_date."', days = '".$number_days->days."',custom_cart_rate = '".$custom_cart_rate."',order_date = '".$order_date."',installation_charges = '".$installation_charges."',amount = '".$amount."',order_status = '".$order_status."' WHERE id = '".$order_id."' ";
        $stmt = $this->con->prepare($query);
        //Executing the statment
         $result = $stmt->execute();
        if ($result) {
            //Insert success
            $responseData['status'] = "success";
            $responseData['message'] = "Success! Order Updated";
        } else {
            //Insert fail
            $responseData['status'] = "error";
            $responseData['message'] = "Error! Something went wrong";
        }
        echo json_encode($responseData);
    }

    public function saveContactPerson($request){
        $id = uniqid('cp_');
        $name = $request->getParam('name');
        $company_id = $request->getParam('company_id');
        $contact_number = $request->getParam('contact_number');
        $email_id = $request->getParam('email_id');
        $created_at = date("Y-m-d H:i:s");
        $checkquery = "SELECT id FROM contact_persons WHERE email = '".$email_id."' ";
        $stmt = $this->con->prepare($checkquery);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        
        if($result)
            $query = "UPDATE contact_persons SET name = '".$name."',email = '".$email_id."',company_id = '".$company_id."',contact_number = '".$contact_number."' WHERE id = '".$result['id']."' ";
        else 
            $query = "INSERT INTO contact_persons(id, name, email, company_id, contact_number, created_at) VALUES('$id', '$name', '$email_id', '$company_id', '$contact_number','$created_at')";

        /*echo $query;
        exit();*/
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $result = $stmt->execute();
        if ($result) {
            //Insert success
            $responseData['status'] = "success";
            $responseData['message'] = "Success! Contact Person created";
        } else {
            //Insert fail
            $responseData['status'] = "error";
            $responseData['message'] = "Error! Something went wrong";
        }
        echo json_encode($responseData);
    }


    public function savePO($request){
        $id = uniqid('po_');
        $order_id = $request->getParam('order_list');
        $company_id = $request->getParam('company_id');
        $administrator_id = $request->getParam('administrator_id');
        $po_number = $request->getParam('po_number');
        $po_amount = $request->getParam('po_amount');
        $img_url = $request->getParam('img_url');
        $created_at = date("Y-m-d H:i:s");
        $query = "INSERT INTO po_received(id, company_id, administrator_id, po_number, po_amount, img_url, created_at) VALUES('$id', '$company_id', '$administrator_id', '$po_number', '$po_amount', '$img_url', '$created_at')";
        //echo $query;
        
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $result = $stmt->execute();

        //Update po_id in order_info table
        for ($i=0; $i < count($order_id); $i++) { 
            $updateOrderQuery = "UPDATE order_info SET po_id = '".$id."' WHERE id = '".$order_id[$i]."' "; 
            $stmt = $this->con->prepare($updateOrderQuery);
            //Executing the statment
            $result = $stmt->execute();
        }
        if ($result) {
            //Insert success
            $responseData['status'] = "success";
            $responseData['message'] = "Success! PO created";
        } else {
            //Insert fail
            $responseData['status'] = "error";
            $responseData['message'] = "Error! Something went wrong";
        }
        echo json_encode($responseData);
        //Closing the statment
        $this->con = null;
        exit();
    }
    public function getAllSalespersons($request){
        $query = "SELECT `id`, `first_name`, `last_name`,CONCAT(`first_name`,' ',`last_name`) fullname , `email_id`, `contact_number`, `address`, `joining_date`, `status` FROM `sales_person`";
        //Crating an statement
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $responseData = array();
        if(count($result)){
            $responseData['status'] = "success";
        }else{
            $responseData['status'] = "error";
            $responseData['message'] = "Fail to get Sales Person List/ Sales Person list not available.";
        }
        $responseData['list'] = $result;
        echo json_encode($responseData);
        //Closing the statment
        $this->con = null;
        exit();

    }
    public function getAllSites($request){
        $query = "SELECT `id`, `location_name`, `site_code`, `size`, `cart_rate`, `min_cart_rate`, `landmark` FROM `site_info`";
        //Crating an statement
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $responseData = array();
        if(count($result)){
            $responseData['status'] = "success";
        }else{
            $responseData['status'] = "error";
            $responseData['message'] = "Fail to get Sites List/ Sites list not available.";
        }
        $responseData['list'] = $result;
        echo json_encode($responseData);
        //Closing the statment
        $this->con = null;
        exit();

    }
    public function getAllOrders($request){
        //$query = "select t1.id orderid,t2.`id` customer_id,t2.party_name customer_name,t3.id sp_id,CONCAT(t3.`first_name`,' ',t3.`last_name`) spfullname,t4.id site_id,t4.location_name,t4.site_code,t5.po_number,t5.img_url  from order_info t1 inner join companies t2 on t1.customer_id = t2.id inner join sales_person t3 on t3.id = t1.sales_person_id inner join site_info t4 on t4.id = t1.site_id left join po_received t5 on t4.id = t5.order_id";
        $query = "SELECT 
            t1.id orderid, t1.`amount` order_amount,
            t2.`id` customer_id, t2.party_name customer_name,
            t3.id sp_id, CONCAT(t3.`first_name`,' ',t3.`last_name`) spfullname,
            t4.id site_id, t4.location_name, t4.site_code, t5.po_number, t5.img_url, t5.po_amount
            FROM
            order_info t1 inner join companies t2 on t1.customer_id = t2.id
            inner join sales_person t3 on t3.id = t1.sales_person_id
            inner join site_info t4 on t4.id = t1.site_id
            left join po_received t5 on t1.po_id = t5.id";
        //Crating an statement
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $responseData = array();
        if(count($result))
            $responseData['status'] = "success";
        else
            $responseData['status'] = "fail";
        $responseData['list'] = $result;
        //echo json_encode($responseData);
        echo json_encode($result);
        //Closing the statment
        $this->con = null;
        exit();
    }
    public function loginToRS($request){
        $email_id = $request->getParam('email_id');
        $password = $request->getParam('password');
        $type_of_login = $request->getParam('type_of_login');

        //check use is exist or not with respective type of login
        if($email_id === "" || $type_of_login === "" || $password === ""){
            echo '{"status":"error","message":"Some fields are missing."}';
            return;
        }
        switch ($type_of_login) {
            case 'master_admin':
                $query = "SELECT * from master_admin WHERE email_id = '$email_id'";
                break;
            case 'administrator':
                $query = "SELECT * from administrator WHERE email_id = '$email_id'";
                break;
            case 'sales_person':
                $query = "SELECT * from sales_person WHERE email_id = '$email_id'";
                break;
            }
        $stmt = $this->con->query($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $num_rows = count($result);
        if($num_rows > 0){//user exist
            //printf($result);
            $hashed_password = $result[0]['password'];
            if (password_verify($password, $hashed_password)){                         
                echo '{"status":"success","message":"login Successfully","detail":'.json_encode($result[0]).'}';            
            }else{
                echo '{"status":"error","message":"Please check email_id and password."}';
            }
        }else{//user is not exits
            //printf($result);
            echo '{"status":"error","message":"email_id not Registered with us."}';
        }
    }

    //TAX related
    public function getAllTaxInfo($request){
        $type = $request->getAttribute('company_id');
        $query = "SELECT * FROM tax_info";
        switch ($type) {
            case 'active':
                $query.=" WHERE status = 1";
                break;
            case 'disabled':
                $query.=" WHERE status = 0";
                break;
        }
        $stmt = $this->con->query($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $num_rows = count($result);
        if($num_rows > 0){
            echo '{"status":"success","tax_list":'.json_encode($result).'}';
        }else{
            echo '{"status":"error","message":"Tax information not available."}';
        }
    }

    //create new TAX status-enable/disable
    public function saveTaxInfo($request){
        $tax_type = $request->getParam('tax_type');
        $amount_in_percent = $request->getParam('amount_in_percent');
        $description = $request->getParam('description');
        $status = $request->getParam('tax_status');
        if($status == ''){
            $status = "disabled";
        }
        $created_at = date("Y-m-d H:i:s");
        $id = uniqid('tax_');

        $query = "INSERT INTO tax_info(id, tax_type, amount_in_percent, description, status, created_at) VALUES ('$id', '$tax_type', '$amount_in_percent', '$description', '$status', '$created_at')";

        //echo $query;
        $stmt = $this->con->prepare($query);
        //Executing the statment
        $result = $stmt->execute();
        if($result){
            echo '{"status":"success","message":"Successfully added."}';
        }else{
            echo '{"status":"error","message":"Error while adding new tax info. please try again later."}';
        }
    }

    //update tax info
    public function updateTaxInfo($request){    
        $tax_id = $request->getParam('tax_id');
        $check_query = "SELECT * FROM tax_info WHERE id = '$tax_id'";
        //echo $check_query;
        $stmt = $this->con->query($check_query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $num_rows = count($result);
        //echo $result;
        if($num_rows > 0){//id exist go for updation
            $tax_type = $request->getParam('tax_type');
            $amount_in_percent = $request->getParam('amount_in_percent');
            $description = $request->getParam('description');
            $status = $request->getParam('tax_status');
            $query = "UPDATE tax_info SET tax_type='$tax_type',amount_in_percent='$amount_in_percent',description='$description',status='$status' WHERE id='$tax_id'";
            $stmt = $this->con->prepare($query);
            //Executing the statment
            $result = $stmt->execute();
            if ($result) {
            //Insert success
                $responseData['status'] = "success";
                $responseData['message'] = "Success! Tax changes updated Successfully.";
            } else {
            //Insert fail
                $responseData['status'] = "error";
                $responseData['message'] = "Error! Something went wrong Please refresh and try again.";
            }
            echo json_encode($responseData);
        }else{
            echo '{"status":"error","message":"tax id not available."}';
        }

    }
    
}
