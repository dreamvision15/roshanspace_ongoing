-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2016 at 04:40 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `roshanspace`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `id` varchar(45) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email_id` varchar(110) NOT NULL,
  `password` varchar(124) NOT NULL,
  `contact_number` varchar(45) DEFAULT NULL,
  `address` text,
  `status` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`id`, `first_name`, `last_name`, `email_id`, `password`, `contact_number`, `address`, `status`, `created_at`, `last_login`) VALUES
('ad_14534', 'Jane', 'Doe', 'janedoe@example.com', 'testplanepass', '9800452122', 'test street,doesnot exists address', 1, '2016-09-20 16:20:23', NULL),
('ad_57f2039b24116', 'Test', 'User', 'testuser@gmail.com', '$2y$10$hs0SlXmOmeN.r1Ejra6LzezJhh.MBWbRKDN2q9lOdvJwopXzG/Qgy', '9869286465', 'test', 0, '2016-10-03 09:07:07', NULL),
('ad_57f20411af965', 'Test', 'User', 'testuser1@gmail.com', '$2y$10$wdE5uEhkAH8QYSHNcZX5q.zLfjfa.OHi7DHuXZBy2u2INVLZPmNEe', '9869286465', 'test user', 0, '2016-10-03 09:09:05', NULL),
('ad_58063a5e16943', 'asdf', 'qwerty', 'ad@roshanspace.com', '$2y$10$zqT.//uLuxz8uINM7LC6beCczld5PaZj1IjoT2TsidydSc9x/Xhu.', '9096228877', 'sample address', 0, '2016-10-18 17:06:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` varchar(45) NOT NULL,
  `party_name` varchar(110) DEFAULT NULL,
  `address` text,
  `office_number` varchar(45) DEFAULT NULL,
  `other_number` varchar(45) DEFAULT NULL,
  `PAN_number` varchar(45) DEFAULT NULL,
  `email_id` varchar(110) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `party_name`, `address`, `office_number`, `other_number`, `PAN_number`, `email_id`, `created_at`) VALUES
('ci_14532', 'Test Party1', 'test street,doesnot exists address', '0226523604', '02226524291', 'AYVXZ6254T', 'janedoe@example.com', '2016-09-20 16:35:04'),
('cust_57ff660a885a8', 'test2', 'test add here', '02245456595', '02245456565', 'AOLK9452F', 'testuser@gmail.com', '2016-10-13 12:46:34'),
('cust_58063b9a25e35', 'MOMS pvt limited', 'sample address of moms', '022-2345685', 'undefined', 'BMAPD8989J', 'info@momspvt.com', '2016-10-18 17:11:22');

-- --------------------------------------------------------

--
-- Table structure for table `contact_persons`
--

CREATE TABLE `contact_persons` (
  `id` varchar(110) CHARACTER SET utf8 NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `contact_number` varchar(255) CHARACTER SET utf8 NOT NULL,
  `company_id` varchar(45) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_persons`
--

INSERT INTO `contact_persons` (`id`, `name`, `email`, `contact_number`, `company_id`, `created_at`) VALUES
('cp_57ff3990c0eea', 'test contact person', 'testcontact@person.com', '9898546563', 'ci_14532', '2016-10-13 09:36:48'),
('cp_58065b9345144', 'Rakesh Sharma', 'rakesh@momspvt.com', '', 'cust_58063b9a25e35', '2016-10-18 19:27:47'),
('cp_5807e3aaa2867', 'test2', 'asd@momspvt.com', '123456789', 'cust_58063b9a25e35', '2016-10-19 23:20:42');

-- --------------------------------------------------------

--
-- Table structure for table `master_admin`
--

CREATE TABLE `master_admin` (
  `id` varchar(45) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email_id` varchar(110) DEFAULT NULL,
  `password` varchar(124) DEFAULT NULL,
  `contact_number` varchar(45) DEFAULT NULL,
  `address` text,
  `created_at` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_admin`
--

INSERT INTO `master_admin` (`id`, `first_name`, `last_name`, `email_id`, `password`, `contact_number`, `address`, `created_at`, `last_login`) VALUES
('ma_57ee0ca5c0c09', 'john', 'doe', 'johndoe@example.com', '$2y$10$0W6wmQL7lfUaAsWAh8Q22.PfpD8hTUeufzde6vsMWgh0XVGaJaTfq', '9869065212', 'test address here', '2016-09-30 08:56:37', NULL),
('ma_58063ada4a68b', 'asdf', 'qwerty', 'ma@roshanspace.com', '$2y$10$63BzOs4F6/8.99Py2Gv10.GQ7y3nv7heUAtPh6Nvqagr805H/tLOa', '9096228877', 'sample address', '2016-10-18 17:08:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_info`
--

CREATE TABLE `order_info` (
  `id` varchar(45) NOT NULL,
  `customer_id` varchar(45) DEFAULT NULL,
  `sales_person_id` varchar(45) DEFAULT NULL,
  `cp_id` varchar(110) DEFAULT NULL,
  `site_id` varchar(110) NOT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  `custom_cart_rate` varchar(45) DEFAULT NULL,
  `order_date` date DEFAULT NULL,
  `installation_charges` varchar(45) DEFAULT NULL,
  `amount` varchar(110) DEFAULT NULL,
  `order_status` tinyint(1) DEFAULT NULL,
  `po_id` varchar(45) DEFAULT NULL,
  `img_url` varchar(512) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_info`
--

INSERT INTO `order_info` (`id`, `customer_id`, `sales_person_id`, `cp_id`, `site_id`, `from_date`, `to_date`, `days`, `custom_cart_rate`, `order_date`, `installation_charges`, `amount`, `order_status`, `po_id`, `img_url`, `created_at`) VALUES
('ord_5832d2414008b', 'ci_14532', 'sp_58063afd77e7b', 'cp_57ff3990c0eea', 'st_57f35f428c453', '2016-11-01', '2016-11-09', 9, '50000', '2016-11-01', '1200', '16200', 0, NULL, NULL, '2016-11-21 11:53:53'),
('ord_583306430cb9f584614157', 'ci_14532', 'sp_58063afd77e7b', 'cp_57ff3990c0eea', 'st_57f35f428c453', '2016-12-20', '2016-12-24', 5, '50000', '2016-11-01', '200', '8533.3333333333', 0, NULL, NULL, '2016-11-21 15:35:47'),
('ord_583306430cb9f75364494', 'ci_14532', 'sp_58063afd77e7b', 'cp_57ff3990c0eea', 'st_123256', '2016-11-01', '2016-11-05', 5, '50000', '2016-11-01', '1000', '9333.3333333333', 0, NULL, NULL, '2016-11-21 15:35:47'),
('ord_583306a65ab151875564488', 'cust_58063b9a25e35', 'sp_58063afd77e7b', 'cp_58065b9345144', 'st_123256', '2016-12-01', '2016-12-30', 30, '40000', '2016-11-02', '1000', '41000', 0, NULL, NULL, '2016-11-21 15:37:26'),
('ord_583306a65ab1579018687', 'cust_58063b9a25e35', 'sp_58063afd77e7b', 'cp_5807e3aaa2867', 'st_57f35f428c453', '2016-12-20', '2016-12-22', 3, '50000', '2016-11-01', '3000', '8000', 0, NULL, NULL, '2016-11-21 15:37:26');

-- --------------------------------------------------------

--
-- Table structure for table `po_received`
--

CREATE TABLE `po_received` (
  `id` varchar(45) NOT NULL,
  `company_id` varchar(45) NOT NULL,
  `administrator_id` varchar(45) DEFAULT NULL,
  `po_number` varchar(45) NOT NULL,
  `po_amount` varchar(45) NOT NULL,
  `img_url` varchar(512) NOT NULL,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sales_person`
--

CREATE TABLE `sales_person` (
  `id` varchar(45) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email_id` varchar(110) DEFAULT NULL,
  `password` varchar(120) DEFAULT NULL,
  `contact_number` varchar(45) DEFAULT NULL,
  `address` text,
  `joining_date` date DEFAULT NULL,
  `leaving_date` date DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sales_person`
--

INSERT INTO `sales_person` (`id`, `first_name`, `last_name`, `email_id`, `password`, `contact_number`, `address`, `joining_date`, `leaving_date`, `status`, `created_at`, `last_login`) VALUES
('sp_14536', 'Jane', 'Doe', 'janedoe@example.com', 'testplanepass', '9800452122', 'test street,doesnot exists address', '2016-08-25', NULL, 1, '2016-09-20 16:34:33', NULL),
('sp_57f36710c5559', 'Test', 'User', 'omkar.bandkar@gmail.com', '$2y$10$yz2jl.HsTPNr8gquoZBrbuo09Qi84GUcrAqFz9pNAjWA4vM.fT0ki', '1234567891', 'hjkhkijkh', '0000-00-00', '0000-00-00', 0, '2016-10-04 10:23:44', NULL),
('sp_58063afd77e7b', 'asdf', 'qwerty', 'sp@roshanspace.com', '$2y$10$2mdRMH1kFFwn6li2mfMix.Uym3qYtgRpA4pi7dUbqaEkeUtxQwfMu', '9096228877', 'sample address', '0000-00-00', '0000-00-00', 0, '2016-10-18 17:08:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `site_info`
--

CREATE TABLE `site_info` (
  `id` varchar(110) NOT NULL,
  `location_name` varchar(110) DEFAULT NULL,
  `site_code` varchar(45) DEFAULT NULL,
  `size` varchar(45) DEFAULT NULL,
  `cart_rate` varchar(45) DEFAULT NULL,
  `min_cart_rate` varchar(45) DEFAULT NULL,
  `landmark` text,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_info`
--

INSERT INTO `site_info` (`id`, `location_name`, `site_code`, `size`, `cart_rate`, `min_cart_rate`, `landmark`, `created_at`) VALUES
('st_123256', 'Thane', 'TN001', 'LARGE', '50000', '40000', 'Stn Road', '2016-09-20 16:39:46'),
('st_57f35f428c453', 'Kunj Vihar', 'TN002', 'LARGE', '50000', '40000', 'Kunj Vihar', '2016-10-30 20:22:18'),
('st_57f360b5ccc0e', 'Kunj Vihar', 'TN003', 'LARGE', '50000', '40000', 'Kunj Vihar', '2016-10-30 20:15:07');

-- --------------------------------------------------------

--
-- Table structure for table `tax_info`
--

CREATE TABLE `tax_info` (
  `id` int(11) NOT NULL,
  `tax_type` varchar(110) DEFAULT NULL,
  `amount_in_percent` int(11) DEFAULT NULL,
  `description` text,
  `status` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_persons`
--
ALTER TABLE `contact_persons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_admin`
--
ALTER TABLE `master_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_info`
--
ALTER TABLE `order_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_order_info_customer_id_idx` (`customer_id`),
  ADD KEY `fk_order_info_sales_person_id_idx` (`sales_person_id`),
  ADD KEY `fk_order_info_site_id_idx` (`site_id`),
  ADD KEY `cp_id` (`cp_id`),
  ADD KEY `po_id` (`po_id`);

--
-- Indexes for table `po_received`
--
ALTER TABLE `po_received`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_po_reveived_order_id_idx` (`company_id`),
  ADD KEY `fk_po_reveived_administrator_id_idx` (`administrator_id`);

--
-- Indexes for table `sales_person`
--
ALTER TABLE `sales_person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_info`
--
ALTER TABLE `site_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax_info`
--
ALTER TABLE `tax_info`
  ADD PRIMARY KEY (`id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `order_info`
--
ALTER TABLE `order_info`
  ADD CONSTRAINT `fk_order_info_company_id` FOREIGN KEY (`customer_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_order_info_cp_id` FOREIGN KEY (`cp_id`) REFERENCES `contact_persons` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_info_po_id` FOREIGN KEY (`po_id`) REFERENCES `po_received` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_info_sales_person` FOREIGN KEY (`sales_person_id`) REFERENCES `sales_person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_info_site_id` FOREIGN KEY (`site_id`) REFERENCES `site_info` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `po_received`
--
ALTER TABLE `po_received`
  ADD CONSTRAINT `fk_po_reveived_administrator_id` FOREIGN KEY (`administrator_id`) REFERENCES `administrator` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
