




=====
1) Get pending PO records
=====
select t1.id orderid,
t1.from_date,
t1.to_date,
t3.id customerid,
t3.party_name,
t4.id salespersonid,CONCAT(t4.first_name,'',t4.last_name) salesperson,
t5.id site_id,
t5.location_name,
t5.site_code,
t6.id,
t6.name,
t6.email 
from
order_info t1 inner join companies t3 on t3.id = t1.customer_id 
inner join 
sales_person t4 on t4.id = t1.sales_person_id 
inner join 
site_info t5 on t5.id = t1.site_id 
inner join
contact_persons t6 on t1.cp_id = t6.id
where t1.po_id is null

// left join 
// po_received t2 on t1.po_id = t2.id 
// where t2.id is null

=====
2) Get all orders
=====
select t1.id orderid,
t2.`id` customer_id,
t2.party_name customer_name,
t3.id sp_id,CONCAT(t3.`first_name`,' ',t3.`last_name`) spfullname,

t4.id site_id,
t4.location_name,
t4.site_code,
t5.po_number,
t5.img_url 
from 
order_info t1 inner join companies t2 on t1.customer_id = t2.id 
inner join 
sales_person t3 on t3.id = t1.sales_person_id 
inner join 
site_info t4 on t4.id = t1.site_id 
left join 
po_received t5 on t1.po_id = t5.id


========
3) Get received po list
========

SELECT t1.id as order_id, t1.amount as order_amount, 
t1.from_date, 
t1.to_date, 
t2.id as po_id, 
t2.po_number, 
t2.po_amount, 
t2.img_url, 
t3.id as company_id, 
t3.party_name, 
t4.name as cp_name, 
t5.location_name, 
t5.site_code, t5.size 
FROM 
order_info t1 INNER JOIN po_received t2 on t1.po_id = t2.id 
INNER JOIN 
companies t3 on t3.id = t1.customer_id 
INNER JOIN 
contact_persons t4 on t4.id = t1.cp_id 
INNER JOIN 
site_info t5 on t1.site_id = t5.id
