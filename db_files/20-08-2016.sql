SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `roshanspace` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `roshanspace` ;

-- -----------------------------------------------------
-- Table `roshanspace`.`master_admin`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `roshanspace`.`master_admin` ;

CREATE TABLE IF NOT EXISTS `roshanspace`.`master_admin` (
  `id` VARCHAR(45) NOT NULL,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  `email_id` VARCHAR(110) NULL,
  `password` VARCHAR(124) NULL,
  `contact_number` VARCHAR(45) NULL,
  `address` TEXT NULL,
  `created_at` DATETIME NULL,
  `last_login` DATETIME NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

-- PHP QUERY -> INSERT INTO master_admin(`first_name`,`last_name`,`email_id`,`password`,`contact_number`,`address`,`created_at`) VALUES('John','Doe','johndoe@example.com','testplanepass','9800452122','test street,doesnot exists address','"'.date("Y-m-d H:i:s").'"');
-- Mysql Query -> 
-- INSERT INTO master_admin(`first_name`,`last_name`,`email_id`,`password`,`contact_number`,`address`,`created_at`) VALUES('John','Doe','johndoe@example.com','testplanepass','9800452122','test street,doesnot exists address',now());
-- -----------------------------------------------------
-- Table `roshanspace`.`administrator`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `roshanspace`.`administrator` ;

CREATE TABLE IF NOT EXISTS `roshanspace`.`administrator` (
  `id` VARCHAR(45) NOT NULL,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  `email_id` VARCHAR(110) NOT NULL,
  `password` VARCHAR(124) NOT NULL,
  `contact_number` VARCHAR(45) NULL,
  `address` TEXT NULL,
  `status` TINYINT(1) NULL DEFAULT 0,
  `created_at` DATETIME NULL,
  `last_login` DATETIME NULL,
  PRIMARY KEY (`id`))
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;
-- This query sets the default status to 1 instead of 0 (0 -Inactive, 1 -Active)
-- PHP QUERY -> INSERT INTO administrator(`id`,`first_name`,`last_name`,`email_id`,`password`,`contact_number`,`address`,`status`,`created_at`) VALUES('ad_14534','Jane','Doe','janedoe@example.com','testplanepass','9800452122','test street,doesnot exists address',1,'"'.date("Y-m-d H:i:s").'"');
-- INSERT INTO administrator(`id`,`first_name`,`last_name`,`email_id`,`password`,`contact_number`,`address`,`status`,`created_at`) VALUES('ad_14534','Jane','Doe','janedoe@example.com','testplanepass','9800452122','test street,doesnot exists address',1,now());
-- -----------------------------------------------------
-- Table `roshanspace`.`sales_person`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `roshanspace`.`sales_person` ;

CREATE TABLE IF NOT EXISTS `roshanspace`.`sales_person` (
  `id` VARCHAR(45) NOT NULL,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  `email_id` VARCHAR(110) NULL,
  `password` VARCHAR(120) NULL,
  `contact_number` VARCHAR(45) NULL,
  `address` TEXT NULL,
  `joining_date` DATE NULL,
  `leaving_date` DATE NULL,
  `status` TINYINT(1) NULL DEFAULT 0,
  `created_at` DATETIME NULL,
  `last_login` DATETIME NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;
-- the sales person joining date is 25th Aug 2016
-- PHP QUERY -> INSERT INTO sales_person(`id`,`first_name`,`last_name`,`email_id`,`password`,`contact_number`,`address`,`joining_date`,`status`,`created_at`) VALUES('sp_14536','Jane','Doe','janedoe@example.com','testplanepass','9800452122','test street,doesnot exists address','2016-08-25',1,'"'.date("Y-m-d H:i:s").'"');

-- INSERT INTO sales_person(`id`,`first_name`,`last_name`,`email_id`,`password`,`contact_number`,`address`,`joining_date`,`status`,`created_at`) VALUES('sp_14536','Jane','Doe','janedoe@example.com','testplanepass','9800452122','test street,doesnot exists address','2016-08-25',1,now());

-- -----------------------------------------------------
-- Table `roshanspace`.`customer_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `roshanspace`.`customer_info` ;

CREATE TABLE IF NOT EXISTS `roshanspace`.`customer_info` (
  `id` VARCHAR(45) NOT NULL,
  `party_name` VARCHAR(110) NULL,
  `address` TEXT NULL,
  `office_number` VARCHAR(45) NULL,
  `other_number` VARCHAR(45) NULL,
  `PAN_number` VARCHAR(45) NULL,
  `contact_person_name` VARCHAR(110) NULL,
  `email_id` VARCHAR(110) NULL,
  `contact_person_number` VARCHAR(45) NULL,
  `created_at` DATETIME NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

-- PHP Query -> INSERT INTO customer_info(`id`,`party_name`,`address`,`office_number`,`other_number`,`PAN_number`,`contact_person_name`,`email_id`,`contact_person_number`,`created_at`) VALUES('ci_14532','Test Party1','test street,doesnot exists address','0226523604','02226524291','AYVXZ6254T','washington sundar','janedoe@example.com','9800452122','"'.date("Y-m-d H:i:s").'"' )
-- INSERT INTO customer_info(`id`,`party_name`,`address`,`office_number`,`other_number`,`PAN_number`,`contact_person_name`,`email_id`,`contact_person_number`,`created_at`) VALUES('ci_14532','Test Party1','test street,doesnot exists address','0226523604','02226524291','AYVXZ6254T','washington sundar','janedoe@example.com','9800452122',now() );
-- -----------------------------------------------------
-- Table `roshanspace`.`tax_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `roshanspace`.`tax_info` ;

CREATE TABLE IF NOT EXISTS `roshanspace`.`tax_info` (
  `id` INT NOT NULL,
  `tax_type` VARCHAR(110) NULL,
  `amount_in_percent` INT NULL,
  `description` TEXT NULL,
  `status` TINYINT(1) NULL DEFAULT 0,
  `created_at` DATETIME NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `roshanspace`.`site_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `roshanspace`.`site_info` ;

CREATE TABLE IF NOT EXISTS `roshanspace`.`site_info` (
  `id` VARCHAR(110) NOT NULL,
  `location_name` VARCHAR(110) NULL,
  `site_code` VARCHAR(45) NULL,
  `size` VARCHAR(45) NULL,
  `cart_rate` VARCHAR(45) NULL,
  `min_cart_rate` VARCHAR(45) NULL,
  `landmark` TEXT NULL,
  `created_at` DATETIME NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `roshanspace`.`order_info`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `roshanspace`.`order_info` ;

CREATE TABLE IF NOT EXISTS `roshanspace`.`order_info` (
  `id` VARCHAR(45) NOT NULL,
  `customer_id` VARCHAR(45) NULL,
  `sales_person_id` VARCHAR(45) NULL,
  `site_id` VARCHAR(110) NOT NULL,
  `from_date` DATE NULL,
  `to_date` DATE NULL,
  `days` INT NULL,
  `custom_cart_rate` VARCHAR(45) NULL,
  `order_date` DATE NULL,
  `installation_charges` VARCHAR(45) NULL,
  `amount` VARCHAR(110) NULL,
  `order_status` TINYINT(1) NULL,
  `po_status` TINYINT(1) NULL,
  `img_url` VARCHAR(512) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_order_info_customer_id_idx` (`customer_id` ASC),
  INDEX `fk_order_info_sales_person_id_idx` (`sales_person_id` ASC),
  INDEX `fk_order_info_site_id_idx` (`site_id` ASC),
  CONSTRAINT `fk_order_info_customer_id`
    FOREIGN KEY (`customer_id`)
    REFERENCES `roshanspace`.`customer_info` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_order_info_sales_person_id`
    FOREIGN KEY (`sales_person_id`)
    REFERENCES `roshanspace`.`sales_person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_order_info_site_id`
    FOREIGN KEY (`site_id`)
    REFERENCES `roshanspace`.`site_info` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

-- INSERT INTO order_info(`id`,`customer_id`,`sales_person_id`,`site_id`,`from_date`,`to_date`,`days`,`custom_cart_rate`,`order_date`,`installation_charges`,`amount`,`order_status`) VALUES('rs_21453','ci_14532','sp_14536','1','2016-08-30','2016-09-30',30,'100000','2016-08-25','20000','250000',1);
-- -----------------------------------------------------
-- Table `roshanspace`.`po_reveived`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `roshanspace`.`po_reveived` ;

CREATE TABLE IF NOT EXISTS `roshanspace`.`po_reveived` (
  `id` VARCHAR(45) NOT NULL,
  `order_id` VARCHAR(45) NOT NULL,
  `administrator_id` VARCHAR(45) NULL,
  `po_number` VARCHAR(45) NOT NULL,
  `img_url` VARCHAR(512) NOT NULL,
  `created_at` DATE NULL,
  `po_status` VARCHAR(45) NULL COMMENT 'short/excess',
  PRIMARY KEY (`id`),
  INDEX `fk_po_reveived_order_id_idx` (`order_id` ASC),
  INDEX `fk_po_reveived_administrator_id_idx` (`administrator_id` ASC),
  CONSTRAINT `fk_po_reveived_order_id`
    FOREIGN KEY (`order_id`)
    REFERENCES `roshanspace`.`order_info` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_po_reveived_administrator_id`
    FOREIGN KEY (`administrator_id`)
    REFERENCES `roshanspace`.`administrator` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
